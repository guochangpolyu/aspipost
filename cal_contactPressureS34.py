"""
Please run Y4.7.2 -j4 get_forceOnPipe.py and Y4.7.2 -j4 get_pipePostionList.py before running
python3 cal_contactPressureS34.py
"""

import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
from myCommonFunction import *
boundaryDict=reader(boundaryDictPath)
anglePosList=reader(postPath+'/anglePosList.data')
forceOnPipePath='/home/xdjf/s/'
endName='forceOnPipe.data'
f = os.listdir(forceOnPipePath)
f= [i for i in f if endName in i]
f.sort()

savePath='/home/xdjf/s/distribution/'
savePathSvg=savePath+'svg/'
savePathData=savePath+'data/'
checkPath(savePath)
print('data will be saved in '+savePath)
checkPath(savePathSvg)
checkPath(savePathData)

dividedNum=4
dividedDegree=180.0/dividedNum
Y=boundaryDict['aabbmax'][1]-boundaryDict['aabbmin'][1]
totalArea=math.pi*OutDiameter/2.0*Y
centers=np.arange(0.,180.,dividedDegree)
minDegrees=[0.0]+list(centers+dividedDegree/2)
maxDegrees=list(centers+dividedDegree/2)+[180.]
centers=list(centers)+[180.]

theorticalPressures=[abs(simulationPressure*(math.cos(math.radians(i)))**2)+simulationPressure*Knaught*(math.sin(math.radians(i)))**2 for i in centers]

n=0
gap=1
nMax=2000


ppDisplacementList=[]
normalPressureList=[]
shearYPressureList=[]
meanNormalPressureList=[]
meanShearYPressureList=[]
config = {
    "font.family":'Arial',
    "font.size": 16,}
rcParams.update(config)
for subPath in f:
    contactList=[]
    Oiter=subPath.replace(endName,'')
    path=forceOnPipePath+subPath
    forceOnPipeList=reader(path)
    ppPos=reader(forceOnPipePath+Oiter+'ppPos.data')
    for i in forceOnPipeList:
        av=angleVector([i[0][n]-ppPos[n] for n in range(3)])
        force=av.project(i[1])
        contactList.append(list(av)+force)
    normalPressures=[]
    shearYPressures=[]
    for j in range(dividedNum+1):
        minDegree=minDegrees[j]
        maxDegree=maxDegrees[j]
        dividedArea=math.pi*((maxDegree-minDegree)/180)*OutDiameter/2.0*Y
        normalPressure=0.0
        shearYPressure=0.0
        for i in contactList:
            if minDegree<i[0]<=maxDegree:
                normalPressure+=i[3]
                shearYPressure+=i[4]
        normalPressure/=dividedArea*(-1000)
        shearYPressure/=dividedArea*(-1000)
        normalPressures.append(normalPressure)
        shearYPressures.append(shearYPressure)
    normalPressureList.append(normalPressures)
    shearYPressureList.append(shearYPressures)
    meanNormalPressureList.append(np.mean([np.mean([normalPressures[0],normalPressures[-1]])]+normalPressures[1:-1]))
    meanShearYPressureList.append(np.mean([np.mean([shearYPressures[0],shearYPressures[-1]])]+shearYPressures[1:-1]))
    fig1, ax1=plt.subplots(figsize=(10,12),subplot_kw=dict(projection="polar"))
    ax1.plot([math.radians(i) for i in centers], [i/simulationPressure for i in theorticalPressures],label='ALA(2001)')
    ax1.plot([math.radians(i) for i in centers], [i/simulationPressure for i in normalPressures],label=Oiter)
    ax1.set_theta_zero_location('N')
    ax1.set_theta_direction(-1)
    ax1.set_thetamin(0.0)
    ax1.set_thetamax(180.0)
    ax1.set_thetagrids(np.arange(0.0, 181.0, 45.0),['Crown', '        Shoulder', '           Springline', '       Haunch', 'Invert'])
    ax1.set_rgrids([0,0.5,1,1.5,2],['0','0.5','1','1.5','2'])
    plt.ylim(0, 2)
    # plt.legend(loc=1,frameon=False)
    plt.legend(loc=1,bbox_to_anchor=(.9, 1.1),frameon=False)
    ax1.set_aspect('equal')
    plt.tight_layout()
    plt.savefig(savePath+Oiter+"_distribution.png", dpi=300)
    print("save figure: "+savePath+Oiter+"_distribution.png")
    plt.savefig(savePath+Oiter+"_distribution.png")
    plt.close()
n+=1




# fig1, ax1=plt.subplots(figsize=(10,12),subplot_kw=dict(projection="polar"))
# ax1.plot([math.radians(i) for i in centers], [i/simulationPressure for i in theorticalPressures],label='ALA(2001)')
# ax1.plot([math.radians(i) for i in centers], [i/simulationPressure for i in normalPressureList[1]],label='Before pullout')
# ax1.plot([math.radians(i) for i in centers], [i/simulationPressure for i in normalPressureList[30]],label='Peak')
# ax1.set_theta_zero_location('N')
# ax1.set_theta_direction(-1)
# ax1.set_thetamin(0.0)
# ax1.set_thetamax(180.0)
# ax1.set_thetagrids(np.arange(0.0, 181.0, 45.0),['Crown', '        Shoulder', '           Springline', '       Haunch', 'Invert'])
# ax1.set_rgrids([0,0.5,1,1.5,2],['0','0.5','1','1.5','2'])
# plt.ylim(0, 2)
# # plt.legend(loc=1,frameon=False)
# plt.legend(loc=1,bbox_to_anchor=(.9, 1.1),frameon=False)
# ax1.set_aspect('equal')
# plt.tight_layout()
# plt.savefig(savePath+"Total_distribution.png", dpi=300)
# print("save figure: "+savePath+"Total_distribution.png")
# plt.savefig(savePathSvg+"Total_distribution.svg")
# plt.close()
# fig1, ax1=plt.subplots(figsize=(10,10))
# ax1.plot(meanNormalPressureList, meanShearYPressureList)
# plt.ylim(bottom=0)
# plt.xlim(left=0)
# # ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")
# # plt.xlabel('Displacement (mm)')
# # plt.ylabel('Intial distance to pipe center (mm)')
# ax1.set_aspect('equal')
# plt.tight_layout()
# plt.savefig(savePath+"stress_Path.png", dpi=300)
# print("save figure: "+savePath+"stress_Path.png")
# plt.savefig(savePathSvg+"stress_Path.svg")
# # plt.savefig(savePathSvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'.svg')
# plt.close()
# fig1, ax1=plt.subplots(figsize=(10,10))
# ax1.plot(ppDisplacementList, [i[0] for i in normalPressureList],label='Crown')
# ax1.plot(ppDisplacementList, [i[1] for i in normalPressureList],label='Shoulder')
# ax1.plot(ppDisplacementList, [i[2] for i in normalPressureList],label='Springline')
# ax1.plot(ppDisplacementList, [i[3] for i in normalPressureList],label='Haunch')
# ax1.plot(ppDisplacementList, [i[4] for i in normalPressureList],label='Invert')
# plt.legend(loc=0)
# plt.ylim(0,70)
# plt.xlim(0,0.02)
# # ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")
# # plt.xlabel('Displacement (mm)')
# # plt.ylabel('Intial distance to pipe center (mm)')
# ax1.set_aspect('equal')
# plt.tight_layout()
# plt.savefig(savePath+"development.png", dpi=300)
# print("save figure: "+savePath+"development.png")
# plt.savefig(savePathSvg+"development.svg")
# # plt.savefig(savePathSvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'.svg')
# plt.close()


# with open(savePathData+'data.csv','w') as f:
#     f.write('Distance,Crown_Normal,Shoulder_Normal,Springline_Normal,Haunch_Normal,Invert_Normal,Mean_Normal,Crown_Shear,Shoulder_Shear,Springline_Shear,Haunch_Shear,Invert_Shear,Mean_Shear\n')
#     for i in range(len(ppDisplacementList)):
#         f.write(f'{ppDisplacementList[i]},{normalPressureList[i][0]},{normalPressureList[i][1]},{normalPressureList[i][2]},{normalPressureList[i][3]},{normalPressureList[i][4]},{meanNormalPressureList[i]},{shearYPressureList[i][0]},{shearYPressureList[i][1]},{shearYPressureList[i][2]},{shearYPressureList[i][3]},{shearYPressureList[i][4]},{meanShearYPressureList[i]}\n')

# print('save data in '+savePathData+'data.csv')