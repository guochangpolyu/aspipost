'''
get_ file is used for get raw data from Yade
please run python3 get_boundaryInfor.py and python3 cal_importantParticles.pybefore this code
Y4.7.2 -j4 get_centerAndRadius.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import numpy as np
boundaryDict=reader(boundaryDictPath)
centerAndRadiusPath=postPath+'/centerAndRadius/rawData/zone5/'
centerAndRadiusPathZone1=postPath+'/centerAndRadius/rawData/zone1/'
checkPath(centerAndRadiusPath)
checkPath(centerAndRadiusPathZone1)
print(f'_centerAndRadius data will be saved in {centerAndRadiusPath}.')
importantParticlesID=reader(importantParticlesIDPath)
importantParticlesIDinparIDs=[i for i in boundaryDict['parIDs'] if i in importantParticlesID]
importantParticlesIDinzone1IDs=[i for i in boundaryDict['zone1IDs'] if i in importantParticlesID]

n=0
gap=1
cover=True
for path in PPath:
    pathHead=path[path.find('Loadingn')+len('Loadingn'):path.find('Y')].zfill(3)
    if (n%gap==0): # set path gap
        savePath=centerAndRadiusPath+pathHead+'centerAndRadius.data'
        savePathZone1=centerAndRadiusPathZone1+pathHead+'centerAndRadius.data'
        if cover or (not os.path.exists(savePath)) or (not os.path.exists(savePathZone1)):
            print(f'___Yade file: {path} is being loaded.')
            O.load(abspath+'/'+path)
            if cover or (not os.path.exists(savePath)):
                centerAndRadiusList=[[list(O.bodies[id].state.pos),O.bodies[id].shape.radius] for id in importantParticlesIDinparIDs]
                saver(centerAndRadiusList,savePath)
                print(f'______centerAndRadiusList is saved to {savePath}.')
            if cover or (not os.path.exists(savePathZone1)):
                centerAndRadiusZone1List=[[list(O.bodies[id].state.pos),O.bodies[id].shape.radius] for id in importantParticlesIDinzone1IDs]
                saver(centerAndRadiusZone1List,savePathZone1)
                print(f'______centerAndRadiusZone1List is saved to {savePathZone1}.')
    n+=1
