'''
get_ file is used for get raw data from Yade
please run get_boundaryInfor.py before this code
Y4.7.2 -j4 get_posAndRot.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import numpy as np
boundaryDict=reader(boundaryDictPath)

n=0
gap=1
cover=False
for path in PPath:
    pathHead=path[path.find('Loadingn')+len('Loadingn'):path.find('Y')].zfill(3)
    if (n%gap==0): # set path gap
        

        savePath=displacementPath+pathHead+'position.data'
        if cover or (not os.path.exists(savePath)):
            print(f'___Yade file: {path} is being loaded.')
            O.load(abspath+'/'+path)

            disList=[list(b.state.pos) for b in O.bodies]
            saver(disList,savePath)
            print(f'______disList is saved to {savePath}.')
    n+=1


