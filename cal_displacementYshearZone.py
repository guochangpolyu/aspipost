"""
Please run python3 cal_importantParticles.py before running
python3 cal_displacementYshearZone.py
"""

import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
boundaryDict=reader(boundaryDictPath)
anglePosList=reader(postPath+'/anglePosList.data')

f = os.listdir(displacementPath)
f= [i for i in f if 'dis.data' in i]
f.sort()

savePath=postPath+'/displacement/displacementY/shearZone/'
savePathSvg=savePath+'svg/'
savePathData=savePath+'data/'
checkPath(savePath)
print('data will be saved in '+savePath)
checkPath(savePathSvg)
checkPath(savePathData)

importantParticlesID=reader(importantParticlesIDPath)
anglePos=[anglePosList[id][2]*1000 for id in importantParticlesID]

minAnglePos=min(anglePos)
maxAnglePos=max(anglePos)
num=20000

zoneBoundary=[minAnglePos,(0.075*1000+minAnglePos)/2,0.075*1000,0.1*1000,0.15*1000,0.2*1000,maxAnglePos]
zoneNum1=150
zoneNum=[zoneNum1,int(zoneNum1/20.0),int(zoneNum1*1.0/1.5/10),int(zoneNum1*2.0/1.5/1.5/10),int(zoneNum1*2.0/1.5/1.5/1.5/5),int(zoneNum1*12/1.5/1.5/1.5/1.5/5)]
ls=[]
for i in range(len(zoneNum)):
    ls0=np.linspace(zoneBoundary[i],zoneBoundary[i+1],zoneNum[i])
    ls.extend(ls0[:-2])
ls.append(maxAnglePos)
ls=np.array(ls)
bin_centers = (ls[:-1] + ls[1:]) / 2
bin_centers=[i-min(bin_centers) for i in bin_centers]
x=np.array([i for i in anglePos])
Y_MEANSS=[]
titles=[]
n=0
gap=10

from scipy.ndimage import gaussian_filter1d

for subPath in f:
    if (n%gap==0) and n!=0:
        config = {
            "font.family":'Arial',
            "font.size": 16,}
        rcParams.update(config)
        fig1, ax1=plt.subplots(figsize=(3,6))
        ppDisplacement=int(subPath.replace('dis.data',''))/10000.0
        path=displacementPath+subPath
        dis=reader(path)

        dy=[dis[id][1]/0.001 for id in importantParticlesID]

        y=np.array([i for i in dy])
        y_means = []
        for i in range(len(ls) - 1):
            mask = (x >= ls[i]) & (x < ls[i + 1])
            if np.any(mask):  y_means.append(np.mean(y[mask]))
            else: y_means.append(np.nan)
        Y_MEANSS.append(y_means)
        # ax1.scatter(y_means, bin_centers, s=4)
        y_smoothed = gaussian_filter1d(y_means, sigma=2)
        ax1.plot(y_smoothed, bin_centers,linewidth=3)
        maxx=20
        maxy=100
        ticks=np.linspace(0,maxx,6)
        ax1.xaxis.set_ticks(ticks)
        ticksy=np.linspace(0,maxy,11)
        ax1.yaxis.set_ticks(ticksy)
        ax1.set(xlim=[0,maxx], ylim=[0,100])
        titles.append('%.1f'%(float(ppDisplacement)*1000))
        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")
        # plt.xlabel('Displacement (mm)')
        # plt.ylabel('Intial distance to pipe center (mm)')
        # ax1.set_aspect('equal')
        plt.tight_layout()
        plt.savefig(savePath+str(int(float(ppDisplacement)*10000)).zfill(3)+'.png', dpi=300)
        print("save figure: "+savePath+str(int(float(ppDisplacement)*10000)).zfill(3)+".png")
        plt.savefig(savePathSvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'.svg')
        plt.close()
    n+=1 

with open(savePathData+'data.csv','w') as f:
    f.write('Distance')
    for i in titles:
        f.write(f',{i}')
    f.write('\n')
    for n in range(len(bin_centers)):
        f.write(str(bin_centers[n]))
        for i in Y_MEANSS:
            f.write(','+str(i[n]))
        f.write('\n')
print('save data in '+savePathData+'data.csv')