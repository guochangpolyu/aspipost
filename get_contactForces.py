'''
get_ file is used for get raw data from Yade
please run get_boundaryInfor.py before this code
Y4.7.2 -j4 get_contactForces.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *

boundaryDict=reader(boundaryDictPath)

### define and create path for saving
thisSavePath=contactForcePath
checkPath(thisSavePath)
print(f'_contactForce data will be saved in {thisSavePath}.')

n=0
gap=1
cover=False
for path in PPath:
    pathHead=path[path.find('Loadingn')+len('Loadingn'):path.find('Y')].zfill(3)
    if (n%gap==0): # set path gap
        
        print(f'___Yade file: {path} is being loaded.')
        O.load(abspath+'/'+path)

        savePath=thisSavePath+pathHead+'contactForce.data'
        if cover or (not os.path.exists(savePath)):
            contactList=[[i.id1,i.id2,list(i.geom.contactPoint),list(i.phys.normalForce),i.phys.normalForce.norm(),list(i.phys.shearForce),i.phys.shearForce.norm(),list(i.phys.normalForce+i.phys.shearForce),(i.phys.normalForce+i.phys.shearForce).norm()] for i in O.interactions]
            saver(contactList,savePath)
            print(f'______ contactList is saved to {savePath}.')
    n+=1