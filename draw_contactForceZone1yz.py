"""
Please run Y4.7.2 -j4 get_contactForcesZone1.py before running
python3 draw_contactForceZone1yz.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from myCommonFunction import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
ppPosDict=reader(ppPosDictPath)

### define and create path for saving
thisSavePath=contactForceZone1yzFigurePath
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)

### define and get path for reading analysed data
thisReadName=contactForceZone1Path
dataName='contactForce'
fileName = os.listdir(thisReadName)
f = [i for i in fileName if dataName+'.data' in i]
f.sort()
dataName2='maxNormalForces'
maxNormalForces=reader(thisReadName+dataName2+'.data')
maxNormalForceInHistory=max(maxNormalForces)
maxNormalForceInHistory=4
setMinNormalForce=maxNormalForceInHistory*0.05
setMinNormalForce=0.2
### initialize a mesh
thisMesh=mesh_xz(200,boundaryDict,positionList)

### common plt setting
config = {
    "font.family":'Arial',
    "font.size": 16,
    }
colorNum=10
from matplotlib.colors import LinearSegmentedColormap
minGrey=0.8
colors = [(minGrey, minGrey, minGrey), (0, 0, 0)]  # Gray to black
cmap = LinearSegmentedColormap.from_list("custom_gray_black", colors, N=256)
gap=10
n=0
for p in f:
    if (n%gap==0 or n ==1):
        pathHead=p.replace(dataName+'.data','')
        ppPos=ppPosDict[pathHead]
        displacement=int(p.replace(dataName+'.data',''))
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')

        rcParams.update(config)
        fig1 = plt.figure(figsize=(15, 10))
        # from matplotlib.gridspec import GridSpec
        # gs = GridSpec(1, 2, width_ratios=[1, 3])
        fig1, (ax1,ax2) = plt.subplots(1,2,figsize=(9,10),gridspec_kw={'width_ratios': [1, 1], 'wspace': 0.01})
        nn=0
        
        contactList=reader(thisReadName+p)
        # Sort data by force in descending order to plot darker lines first
        contactList.sort(key=lambda x: x[2],reverse=1)
        num=int(len(contactList)*0.4)

        contactList=contactList[:num]
        contactList.sort(key=lambda x: x[2],reverse=0)
        normalForces=[i[2] for i in contactList]
        # meanForce=np.mean(normalForces)
        # print(max(normalForces),meanForce)
        for pos1,pos2,normalForce in contactList:
            # print([pos1[0],pos1[2]],[pos2[0],pos2[2]],normalForce)
            # nn+=1
            # if nn>num: continue
            if normalForce>setMinNormalForce:
                color_value = int((normalForce / maxNormalForceInHistory) * (colorNum-1)) / (colorNum-1)
                pos1[1]=pos1[1]%boundaryDict['cell'][1]
                pos2[1]=pos2[1]%boundaryDict['cell'][1]
                if abs(pos1[1]-pos2[1])>=boundaryDict['cell'][1]*0.9:
                    if pos1[1]>pos2[1]: pos1[1]-=boundaryDict['cell'][1]
                    else: pos2[1]-=boundaryDict['cell'][1]
                ax1.plot(
                    [pos1[1],pos2[1]],[pos1[2],pos2[2]],
                    linewidth=normalForce/maxNormalForceInHistory*8, 
                    # linewidth=1,
                    color=cmap(color_value),
                    zorder=5)
                ax2.plot(
                    [pos1[0],pos2[0]],[pos1[2],pos2[2]],
                    linewidth=normalForce/maxNormalForceInHistory*8, 
                    # linewidth=1,
                    color=cmap(color_value),
                    zorder=5)
        
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=setMinNormalForce, vmax=maxNormalForceInHistory))
        sm.set_array([])
        cbar = plt.colorbar(sm,ax=[ax1,ax2],extend='both',pad=0.02, location='right')
        # cbar = plt.colorbar(sm,extend='max')
        # Set color bar ticks to ten discrete intervals
        ticks=np.linspace(setMinNormalForce, maxNormalForceInHistory, num=colorNum+1)
        cbar.set_ticks(ticks)
        cbar.set_ticklabels([f'{round(tick,2):.2f}' for tick in ticks])
        cbar.cmap.set_under('white')
        cbar.ax.set_position([0.87, 0.1, 0.05, 0.81])
        # cbar.mappable.set_clim(0,math.ceil((simulationPressure*2)/10.0)*10)
        draw_half_circle_outline(ax2, [ppPos[0],ppPos[2]], ppR, 2,edgecolor='black',fill=1)
        ax1.plot([0,boundaryDict['cell'][1]],[ppPos[2]+ppR,ppPos[2]+ppR],linewidth=2,color='black',zorder=6)
        ax1.plot([0,boundaryDict['cell'][1]],[ppPos[2]-ppR,ppPos[2]-ppR],linewidth=2,color='black',zorder=6)
        ax1.set_title('Side view', fontsize=14)
        ax2.set_title('Front view', fontsize=14)
        ax1.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
        ax2.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)

        ax1.set(xlim=[0,boundaryDict['cell'][1]],ylim=thisMesh.abz)
        ax2.set(xlim=thisMesh.abx,ylim=thisMesh.abz)

        for spine in ax1.spines.values():
            spine.set_zorder(15)
        for spine in ax2.spines.values():
            spine.set_zorder(15)
        ax1.set_aspect('equal','box')
        ax2.set_aspect('equal','box')
        plt.subplots_adjust(wspace=0.05)
        
        fig1.suptitle('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm",y=0.95)
        fig1.subplots_adjust(left=-0.1,right=0.83)
        # plt.tight_layout()

        figName=thisSavePath+pathHead
        fig1.savefig(figName+'.png', dpi=300)
        print("save figure: "+figName+'.png')
        fig1.savefig(thisSavePathsvg+pathHead+'.svg')
        plt.close()

    n+=1
