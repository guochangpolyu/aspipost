"""
Please run python3 cal_importantParticles.py before running
python3 draw_displacementYfield_Small.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.colors as colors
from matplotlib.colors import ListedColormap
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
sizeList=reader(sizeListPath)
importantParticlesID=reader(importantParticlesIDPath)

zone1Path=postPath+'/displacement/displacementY/Zone1/'
zone1PathSvg=zone1Path+'svg/'
checkPath(zone1Path)
checkPath(zone1PathSvg)
print(f'Figures of zone 1 are saved in '+zone1Path)

f = os.listdir(displacementPath)
f= [i for i in f if 'dis.data' in i]
f.sort()

thisMesh=mesh_xz(100,boundaryDict,positionList,smallorlarge=0)


gap=10
n=0

x0 = []
z0 = []
size = []
for id in importantParticlesID:
    if id in boundaryDict['zone1IDs']:
        x0.append(positionList[id][0])
        z0.append(positionList[id][2])
        size.append((sizeList[id])**2.5/6.0)


for p in f:
    if (n%gap==0):
        disList=reader(displacementPath+p)
        displacement=int(p.replace('dis.data',''))
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')
        dy = []
        for id in importantParticlesID:
            if id in boundaryDict['zone1IDs']:
                dy.append(disList[id][1]*1000)

        config = {
            "font.family":'Arial',
            "font.size": 16,
        }
        rcParams.update(config)

        fig1, ax1 = plt.subplots(figsize=(6.4,10))
        tick=np.linspace(0,200*0.1,11)
        cmap=plt.get_cmap('viridis')
        newcolors=cmap(np.linspace(0, 1, 256))
        newcmap = ListedColormap(newcolors) 
        # tick=[0, 2, 4, 6, 8,10,12,14,16,18,20]
        # factor=1
        # if (targetSimulation=='S34'): factor=0.3
        # tick1=[i*factor for i in tick]
        bounds = np.linspace(0, max(tick), 21)
        norm = colors.BoundaryNorm(boundaries=bounds, ncolors=160)
        a=ax1.scatter(x0, z0, c=dy, 
                      norm=norm,
                    cmap=newcmap,
                      s=size,alpha=1,edgecolors='none')
        cbar=fig1.colorbar(a, ax=ax1,ticks=tick)
        cbar.mappable.set_clim(0,max(tick))
        cbar.ax.set_yticklabels(['%.1f' %i for i in tick])
        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        plt.xticks([])
        plt.yticks([])

        ax1.set(xlim=thisMesh.abx, ylim=thisMesh.abz)
        ax1.set_aspect('equal')
        plt.tight_layout()
        plt.savefig(zone1Path+str(int(float(ppDisplacement)*10000)).zfill(3)+'.png', dpi=300)
        print("save figure: "+zone1Path+str(int(float(ppDisplacement)*10000)).zfill(3)+'.png')
        plt.savefig(zone1PathSvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'.svg')
        plt. close()
    n+=1
