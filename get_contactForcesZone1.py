'''
get_ file is used for get raw data from Yade
please run get_boundaryInfo.py and cal_importantParticles.py before this code
Y4.7.2 -j4 get_contactForcesZone1.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *

boundaryDict=reader(boundaryDictPath)
importantParticlesID=reader(importantParticlesIDPath)
importantParticlesIDinZone1=[id for id in importantParticlesID if id in boundaryDict['zone1IDs']]
boundaryList=[boundaryDict['wall_id_X0'],boundaryDict['wall_id_X1'],boundaryDict['wall_id_Z0'],boundaryDict['wall_id_Z1']]


### define and create path for saving
thisSavePath=contactForceZone1Path
dataName='contactForce'
checkPath(thisSavePath)
print(f'_ {dataName} data will be saved in {thisSavePath}.')
dataName2='maxNormalForces'
maxNormalForces=[]

n=0
gap=1
cover=False
for path in PPath:
    pathHead=path[path.find('Loadingn')+len('Loadingn'):path.find('Y')].zfill(3)
    if (n%gap==0): # set path gap
        contactList=[]
        savePath=thisSavePath+pathHead+dataName+'.data'
        if cover or (not os.path.exists(savePath)):
            print(f'___Yade file: {path} is being loaded.')
            O.load(abspath+'/'+path)
            maxNormalForce=0.0
            for i in O.interactions:
                if (not (i.id1 in boundaryList)) and (not (i.id2 in boundaryList)) and ((i.id1 in importantParticlesIDinZone1) or (i.id2 in importantParticlesIDinZone1)):
                    pos1=list(O.bodies[i.id1].state.pos)
                    pos2=list(O.bodies[i.id2].state.pos)
                    if i.id1 in boundaryDict['shearbox_members']: pos1=list(i.geom.contactPoint)
                    elif i.id2 in boundaryDict['shearbox_members']: pos2=list(i.geom.contactPoint)
                    normalForce=i.phys.normalForce.norm()
                    # print(i.id1,i.id2)
                    contactList.append([pos1,pos2,normalForce])
                    maxNormalForce=max(maxNormalForce,normalForce)
            maxNormalForces.append(maxNormalForce)
            saver(contactList,savePath)
            print(f'______ {dataName} is saved to {savePath}.')
            saver(maxNormalForces,thisSavePath+dataName2+'.data')
            print(f'______ {dataName2} is saved to {thisSavePath+dataName2}.data')
    n+=1
