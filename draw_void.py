"""
Please run Y4.7.2 -j4 get_centerAndRadius.py before running
python3 draw_void.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from myCommonFunction import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams

boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
ppPosDict=reader(ppPosDictPath)

thisSavePath=voidRatio_zone1_path
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)
print(f'Figures of are saved in '+thisSavePath)

thisReadName=centerAndRadiusPathZone1
checkName= 'centerAndRadius.data'
f = os.listdir(thisReadName)
f= [i for i in f if checkName in i]
f.sort()

thisMesh=mesh_xz(200,boundaryDict,positionList)

gap=10
n=0


config = {
    "font.family":'Arial',
    "font.size": 16,
    }

for p in f:
    if (n%gap==0) or n ==1:
        pathHead=p.replace(checkName,'')
        ppPos=ppPosDict[pathHead]
        displacement=int(p.replace(checkName,''))
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')

        thisMesh.updateSpace([ppPos[0],ppPos[2]],ppR,calTouchedMesh=1)
        voidss=thisMesh.meshList(0.0)

        centerAndRadiusZone1List=reader(centerAndRadiusPathZone1+p)
        for c,r in centerAndRadiusZone1List:
            c=thisMesh.meshilize(periodicialize(c,boundaryDict_cell=boundaryDict['cell'],xyz='xyz'))
            if (0<=c[0]<thisMesh.xMeshNum) and (0<=c[2]<thisMesh.zMeshNum): voidss[int(c[2])][int(c[0])]+=(4.0/3.0) * 3.141592653589793 * (r**3)
        voidss=[[np.nan if thisMesh.space[i][j]==np.nan or thisMesh.space[i][j]==0 else 1-voidss[i][j]/thisMesh.space[i][j] for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]

        window_size=4
        voidss=moving_average_2d_with_weight(np.array(voidss), np.array(thisMesh.space),window_size=window_size)

        rcParams.update(config)
        fig1, ax1 = plt.subplots(figsize=(7,10))

        a=plt.imshow(voidss,interpolation='bilinear',cmap='bwr')

        ppR_meshed=thisMesh.meshilize_length(ppR)
        ppC_meshed=thisMesh.meshilize(ppPos)
        draw_half_circle_outline(ax1, [ppC_meshed[0]-0.5,ppC_meshed[2]-0.5], ppR_meshed, 8.8,edgecolor='black',fill=1)

        cbar = fig1.colorbar(a, ax=ax1,extend='both',ticks=[0.2, 0.3, 0.4, 0.5, 0.6])
        cbar.ax.set_yticklabels(['0.2', '0.3', '0.4', '0.5', '0.6'])
        cbar.mappable.set_clim(.2,.6)

        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        plt.xticks([])
        plt.yticks([])

        ax1.set(xlim=[-.5,thisMesh.xMeshNum-0.5], ylim=[-.5,thisMesh.zMeshNum-0.5])
        ax1.set_aspect('equal')
        plt.tight_layout()

        figName=thisSavePath+pathHead
        plt.savefig(figName+'.png', dpi=300)
        print("save figure: "+figName+'.png')
        plt.savefig(thisSavePathsvg+pathHead+'.svg')

        plt.close()
    n+=1
