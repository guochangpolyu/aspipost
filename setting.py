import sys
sys.path.append('')
from myCommonFunction import *
targetSimulation='S34'
OutDiameter=0.102
ppR=OutDiameter/2.0
targetWidthX=0.6
targetHeightZ1=0.71 # top
targetHeightZ0=0.3 # pipe's center position
thickness=0.01 # bottom boundary thickness
boundaryGap=thickness*1.5
WidthX=targetWidthX+2*thickness+boundaryGap*2
WidthX0=WidthX/2
HeightZ0=targetHeightZ0+boundaryGap+thickness*1 # pipe center's height
HeightZ1=targetHeightZ1+boundaryGap+thickness*1.5 # height of sample

##################################################
simulationPressure=float(''.join([i for i in targetSimulation if i in '0123456789']))
Knaught=1-math.sin(math.radians(39.6))
K0Pressure=Knaught*simulationPressure
mainPaths=dict()
with open('drySPI.txt') as f:
    lines=f.readlines()
    for line in lines:
        mainPaths[line[0:line.find(':')]]=line[line.find(':')+1:].strip()

abspath=mainPaths[targetSimulation]
postPath=abspath+'/post'
boundaryDictPath=postPath+'/boundaryDict.data'
importantParticlesIDPath=postPath+'/importantParticlesID.data'
pressureMatrixPath=postPath+'/pressureMatrix/rawData/'
pressureMatrixZone5Path=postPath+'/pressureMatrixZone5/rawData/'
pressureMatrix_zone1_vertical_path=postPath+'/pressureMatrix/zone1/vertical/'
pressureMatrix_zone1_lateral_path=postPath+'/pressureMatrix/zone1/lateral/'
pressureMatrix_zone5_vertical_path=postPath+'/pressureMatrix/zone5/vertical/'
pressureMatrix_earth_path=postPath+'/pressureMatrix/earth/'
pressureMatrix_zone3_principal_path=postPath+'/pressureMatrix/zone3/principal/'
pressureMatrix_zone5_lateral_path=postPath+'/pressureMatrix/zone5/lateral/'
positionListPath=postPath+'/positionList.data'
internaledPositionListPath=postPath+'/internaledPositionList.data'
voidRatio_zone1_path=postPath+'/Void/Zone1/'
centerAndRadiusPathZone1=postPath+'/centerAndRadius/rawData/zone1/'
ppPosDictPath=postPath+'/pipePostion/sumppPos.data'
contactForcePath=postPath+'/contactForce/rawData/'
contactForceZone1Path=postPath+'/contactForce/rawData/Zone1/'
contactForceDataPath=postPath+'/contactForce/data/'
contactForceFigurePath=postPath+'/contactForce/Fig/'
contactForceZone1FigurePath=postPath+'/contactForce/Fig/Zone1/'
contactForceZone1yzFigurePath=postPath+'/contactForce/Fig/Zone1yz/'
displacement_Y_zone5Path=postPath+'/displacement/displacementY/Zone5/'
displacementXZPath=postPath+'/displacement/displacementXZ/'
displacementPath=postPath+'/displacement/rawData/'
sizeListPath=postPath+'/sizeList.data'
rotation_Y_zone5Path=postPath+'/rotation/rotationY/Zone5/'
rotationPath=postPath+'/rotation/rawData/'
print('**********************************************')
print('Post-analysis of '+targetSimulation+' in '+abspath)

checkPath(postPath+'/paths')
internalPath=list()
externalPath=list()
PPath=list()
for dirpath, dirnames, filenames in os.walk(abspath, topdown=True):
    # 清空 dirnames 列表，这样 os.walk 就不会进入子文件夹
    dirnames.clear()
    for filename in filenames:
        if '/' in filename:
            continue
        if 'timeSaver' in filename:
            continue
        if ".yade.bz2" in filename:
            if 'externaled' in filename:
                externalPath.append(filename)
            elif 'internaled' in filename:
                internalPath.append(filename)
            elif 'P' in filename:
                PPath.append(filename)

PPathNum,PPath=zip(*(sorted(zip([int(i.split('Loadingn')[1].split('Y')[0]) for i in PPath],PPath))))
##################################################