"""
before this code:
myPath.py is needed to operate to get the .path file
calOutAabb is needed to operate to cal particles id who never move out of the aabb
this code is for 
1. calculating the evolution of position and rotation of particles, save to posDict and rotDict whoes keys
   are the displacement of pipe and values are lists of Vector3 class. The results will be saved in 
   postPath+'source_disDict_Cyclic0True.data' and postPath+'source_rotDict_Cyclic0True.data'
2. cal the displacement, save to sumDisplacementDict whoes keys are the displacement of pipe and values are
   dict with keys including 'id'(particle id),'x0','y0','z0'(particle position at path[0]),'dx','dy','dz'
   (particle displacement related to path[0]),'nxy','nxz','nyz'(norm of particle displacement related to path[0])
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from setting import *
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
coordinationDict=reader(postPath+'/coordinationDict.data')
disDict=reader(postPath+'/disDict.data')
figurePath=postPath+f'/displacementYfield_small_{targetSimulation}/'
checkPath(figurePath)
print(f'Figures are saved in '+figurePath)

gap=1
for n in range(len(list(disDict.keys()))):
    if (n%gap==0):
        thisKey=list(disDict.keys())[n]
        print(f'Now analyse the condition of axial displacement is {thisKey}')
        x0 = []
        y0 = []
        z0 = []
        dx = []
        dy = []
        dz = []

        nxy = []
        nxz = []
        nyz = []
        c = []
        paID = boundaryDict['parIDs']
        for id in paID:
            if disDict[thisKey][id][2]>-0.02:
                x0.append(positionList[id][0])
                y0.append(positionList[id][1])
                z0.append(positionList[id][2])
                dx.append(disDict[thisKey][id][0])
                dy.append(disDict[thisKey][id][1])
                dz.append(disDict[thisKey][id][2])
                nxy.append((disDict[thisKey][id][0]**2+disDict[thisKey][id][1]**2)**0.5)
                nxz.append((disDict[thisKey][id][0]**2+disDict[thisKey][id][2]**2)**0.5)
                nyz.append((disDict[thisKey][id][1]**2+disDict[thisKey][id][2]**2)**0.5)
                c.append(coordinationDict[thisKey][id])
            # else:
            #     print(positionList[id])
        aabbx=[boundaryDict['aabbmin'][0],boundaryDict['aabbmax'][0]]
        aabby=[boundaryDict['aabbmin'][1],boundaryDict['aabbmax'][1]]
        aabbz=[boundaryDict['aabbmin'][2],boundaryDict['aabbmax'][2]]
        abx=[boundaryDict['aabbmin'][0],boundaryDict['aabbmin'][0]+0.075]
        abz=[positionList[boundaryDict['shearbox_id']][2]-0.075,positionList[boundaryDict['shearbox_id']][2]+0.075]
        import matplotlib.pyplot as plt
        from matplotlib import rcParams

        config = {
            "font.family":'Arial',
            "font.size": 16,
        }
        rcParams.update(config)

        import matplotlib.colors as colors
        from matplotlib.colors import ListedColormap, LinearSegmentedColormap
        fig1, ax1 = plt.subplots(figsize=(6.4,10))

        rdy=[i*1000 if i>=0 else 0 for i in dy]
        rdy=[i if i<=20 else 20 for i in rdy]
        cmap=plt.get_cmap('binary')
        newcolors=cmap(np.linspace(0, 1, 256))
        newcmap = ListedColormap(newcolors[77:256]) 
        bounds = np.linspace(0, 20, 21)
        norm = colors.BoundaryNorm(boundaries=bounds, ncolors=180)
        # a=ax1.scatter(x0, z0, c=rdy, norm=norm,cmap='Blues',s=2)
        a=ax1.scatter(x0, z0, c=rdy, norm=norm,cmap=newcmap,s=2)
        # a=ax1.scatter(x0, z0, c=rdy, cmap=newcmap,s=2)
        cbar=fig1.colorbar(a, ax=ax1,ticks=[0, 2, 4, 6, 8,10,12,14,16,18,20],extend='max')
        cbar.mappable.set_clim(0,20)
        cbar.ax.set_yticklabels(['0', '2', '4', '6', '8','10','12','14','16','18','20'])
        # ax1.set(xlim=aabbx, ylim=aabbz)
        ax1.set(xlim=abx, ylim=abz)
        ax1.set_title('${u}$ = '+ '%.1f'%(float(thisKey)*1000)+" mm")
        # ax1.spines['left'].set_visible(False)
        # ax1.spines['right'].set_visible(False)
        # ax1.spines['top'].set_visible(False)
        # ax1.spines['bottom'].set_visible(False)
        plt.xticks([])
        plt.yticks([])
        ax1.set_aspect('equal')
        plt.tight_layout()
        plt.savefig(figurePath+str(int(float(thisKey)*10000)).zfill(3)+'.png', dpi=300)
        print("save figure: "+figurePath+str(int(float(thisKey)*10000)).zfill(3)+'.png')

        plt.rcParams['figure.figsize']=(5.68,10)

        ax1.set(xlim=aabbx, ylim=aabbz)
        ax1.set_aspect('equal')
        plt.tight_layout()
        plt.savefig(figurePath+str(int(float(thisKey)*10000)).zfill(3)+'2.png', dpi=300)
        print("save figure: "+figurePath+str(int(float(thisKey)*10000)).zfill(3)+'2.png')
