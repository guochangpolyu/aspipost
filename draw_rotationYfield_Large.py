"""
Please run python3 cal_importantParticles.py before running
python3 draw_rotationYfield_Large.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.colors as colors
from matplotlib.colors import ListedColormap

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
importantParticlesID=reader(importantParticlesIDPath)
sizeList=reader(sizeListPath)

### define and create path for saving
thisSavePath=rotation_Y_zone5Path
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)
print(f'Figures of zone 5 are saved in '+thisSavePath)

### define and get path for reading analysed data
thisReadName=rotationPath
dataName='rot'
f = os.listdir(thisReadName)
f= [i for i in f if dataName+'.data' in i]
f.sort()

### initialize a mesh
thisMesh=mesh_xz(100,boundaryDict,positionList,smallorlarge=0)




config = {
    "font.family":'Arial',
    "font.size": 16,
}


gap=10
n=0

x0 = [positionList[id][0] for id in importantParticlesID]
z0 = [positionList[id][2] for id in importantParticlesID]
size = [(sizeList[id])**2.5/6.0 for id in importantParticlesID]


import numpy as np

def total_rotation_angle(alpha, beta, gamma):
    """
    Calculate the total rotation angle from given angles around x, y, and z axes.
    
    Parameters:
    alpha (float): Rotation angle around x-axis in radians
    beta (float): Rotation angle around y-axis in radians
    gamma (float): Rotation angle around z-axis in radians
    
    Returns:
    float: Total rotation angle in radians
    """
    # Rotation matrix around x-axis
    R_x = np.array([[1, 0, 0],
                    [0, np.cos(alpha), -np.sin(alpha)],
                    [0, np.sin(alpha), np.cos(alpha)]])
    
    # Rotation matrix around y-axis
    R_y = np.array([[np.cos(beta), 0, np.sin(beta)],
                    [0, 1, 0],
                    [-np.sin(beta), 0, np.cos(beta)]])
    
    # Rotation matrix around z-axis
    R_z = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                    [np.sin(gamma), np.cos(gamma), 0],
                    [0, 0, 1]])
    
    # Combined rotation matrix
    R = np.dot(R_z, np.dot(R_y, R_x))
    
    # Calculate the angle of rotation from the rotation matrix
    angle = np.arccos((np.trace(R) - 1) / 2)
    
    return angle




for p in f:
    if (n%gap==0):
        pathHead=p.replace(dataName+'.data','')

        displacement=int(pathHead)
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')

        thisList=reader(thisReadName+p)
        dy = [np.degrees(total_rotation_angle(thisList[id][0],thisList[id][1],thisList[id][2])) for id in importantParticlesID]
        rdy=[(i*1.) for i in dy]

        rcParams.update(config)

        fig1, ax1 = plt.subplots(figsize=(5.68,10))

        # cmap=plt.get_cmap('bone_r')
        # newcolors=cmap(np.linspace(0, 1, 256))
        # newcmap = ListedColormap(newcolors[77:236]) 
        # tick=[0, 2, 4, 6, 8,10,12,14,16,18,20]
        # factor=1
        # if (targetSimulation=='S34'): factor=0.3
        # tick1=[i*factor for i in tick]
        # bounds = np.linspace(0, max(tick1), 21)
        # norm = colors.BoundaryNorm(boundaries=bounds, ncolors=160)
        a=ax1.scatter(x0, z0, c=rdy, 
                    #   norm=norm,cmap=newcmap,
                      s=size,alpha=1,edgecolors='none')

        cbar=fig1.colorbar(a, ax=ax1,
                        #    ticks=tick1,
                           extend='max')
        cbar.mappable.set_clim(0,90)
        # cbar.ax.set_yticklabels(['%.2g' %i for i in tick1])
        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        plt.xticks([])
        plt.yticks([])

        ax1.set(xlim=thisMesh.abx, ylim=thisMesh.abz)
        ax1.set_aspect('equal')

        figName=thisSavePath+pathHead
        plt.savefig(figName+'.png', dpi=300)
        print("save figure: "+figName+'.png')
        plt.savefig(thisSavePathsvg+pathHead+'.svg')
        plt.close()
    n+=1
