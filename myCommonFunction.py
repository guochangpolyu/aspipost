from typing import Any
import math

class angleVector:
    def __init__(self,*data) -> None:
        if len(data)==3:
            self.origin=list(data)
        elif len(data) == 1 and len(data[0]) == 3:
            self.origin=list(data[0])
        else:
            raise ValueError("Input to class angleVector is wrong. Please check.")
        self.angle=math.degrees(math.atan2(self.origin[0],self.origin[2]))
        self.y=self.origin[1]
        self.length=(self.origin[0]**2+self.origin[2]**2)**0.5
        self._data=[self.angle,self.y,self.length]
    def __getitem__(self, index):
        return self._data[index]

    def __setitem__(self, index, value):
        self._data[index] = value
        self.angle=self._data[0]
        self.y=self._data[1]
        self.length=self._data[2]
        self.origin=[math.sin(math.radians(self.angle))*self.length,self.y,math.cos(math.radians(self.angle))*self.length]

    def __len__(self):
        return len(self._data)

    def __repr__(self):
        return repr(self._data)

    def project(self,*another):
        '''
        another is a vector3 class. return a list: 
            list[0] is the project of another on self's extension line, far away is positive; 
            list [1]=another[1]; 
            list[2] is the project of the tangent line of self, right is positive
        '''
        if len(another)==3:
            another=list(another)
        elif len(another) == 1 and len(another[0]) == 3:
            another=list(another[0])
        else:
            raise ValueError("Input to class angleVector.project is wrong. Please check.")
        
        sina=math.sin(math.radians(self.angle))
        cosa=math.cos(math.radians(self.angle))
        return [another[0]*sina+another[2]*cosa,another[1],another[0]*cosa-another[2]*sina]

import numpy as np

def calSphereVolumeInAabb(R, aabbMin, aabbMax, xyz='xyz', num_samples=100000):

    x = np.random.uniform(-R, R, num_samples)
    y = np.random.uniform(-R, R, num_samples)
    z = np.random.uniform(-R, R, num_samples)

    num_sphere=0
    num_inner=0
    for xx,yy,zz in zip(x,y,z):
        if (xx**2 + yy**2 + zz**2) <= R**2:
            num_sphere+=1
            if ((aabbMin[0]<xx<=aabbMax[0]) or (not 'x' in xyz)) and (aabbMin[1]<yy<=aabbMax[1] or (not 'y' in xyz)) and (aabbMin[2]<zz<=aabbMax[2]  or (not 'z' in xyz)):
                num_inner+=1


    volume_fraction =num_inner / num_sphere


    volume = volume_fraction * (4/3) * math.pi * (R)
    return volume

def particleInAabb(center, radius:float,aabbMin,aabbMax):
    if (center[0]-radius)<aabbMin[0]:
        return False
    elif (center[1]-radius)<aabbMin[1]:
        return False
    elif (center[2]-radius)<aabbMin[2]:
        return False
    elif (center[0]+radius)>aabbMax[0]:
        return False
    elif (center[1]+radius)>aabbMax[1]:
        return False
    elif (center[2]+radius)>aabbMax[2]:
        return False
    else: return True


def particleTouchAabb(center, radius:float,aabbMin,aabbMax,xyz='xyz'):
    if (center[0]-radius)<aabbMin[0]<=center[0] or (not 'x' in xyz):
        return True
    elif (center[1]-radius)<aabbMin[1]<=center[1] or (not 'y' in xyz):
        return True
    elif (center[2]-radius)<aabbMin[2]<=center[2] or (not 'z' in xyz):
        return True
    elif (center[0]+radius)>aabbMax[0]>=center[0] or (not 'x' in xyz):
        return True
    elif (center[1]+radius)>aabbMax[1]>=center[1] or (not 'y' in xyz):
        return True
    elif (center[2]+radius)>aabbMax[2]>=center[2] or (not 'z' in xyz):
        return True
    else:
        return False


def sphereVolumeInAabb(center, radius:float,aabbMin,aabbMax, xyz='xyz'):
    if particleTouchAabb(center, radius,aabbMin,aabbMax,xyz):
        return calSphereVolumeInAabb(radius, aabbMin, aabbMax, xyz)
    elif particleInAabb(center, radius,aabbMin,aabbMax):
        return (4.0/3.0) * math.pi * (radius**3)
    else:
        return 0.0


import os,re

def checkPath(path:str):
    """check the path(folder) is existing or not. if not, the folder will be created
    """
    if path!='':
            if not os.path.exists(path):
                print("Path: " + path + " is created.")
                os.makedirs(path)
def savePathFile(pathList:list,filename:str):
    if len(pathList)>0:
        with open(filename,'w') as f:
            for i in pathList:
                f.write(i+'\n')
        print('Paths is saved in: '+filename)
def getPathList(path:str)->list:# avoiding sub-subpath and ordered
    def getIter(element):
        return element[-1]
    ### identify O.iter
    walkPath=list()
    for folders in os.walk(path):
        folders=list(folders)
        folder=folders[0].split(path)[1]
        if '/' in folder:
            continue
        iter=re.findall('[0-9]+',folder)
        if not iter:
            continue 
        if isinstance(iter,list):
            iter=iter[0]
        folders.append(int(iter))
        walkPath.append(folders)
    walkPath.sort(key=getIter)
    return [i[0] for i in walkPath]


def orderPPath(paths:list):
    if len(paths)>0:
        iters=[]
        for path in paths:
            a=int(re.findall(r".*n(.*).*Y",path)[0])
            iters.append(a)
        iters, paths =(list(t) for t in zip(*sorted(zip(iters, paths))))
    return paths


def moving_average_2d(matrix, window_size=3):
    padded_matrix = np.pad(matrix, pad_width=window_size//2, mode='edge')
    smoothed_matrix = np.zeros_like(matrix)

    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if np.isnan(matrix[i, j]):
                # 保持NaN值不变
                smoothed_matrix[i, j] = matrix[i, j]
            else:
                # 计算窗口内的平均值，忽略NaN值
                window = padded_matrix[i:i+window_size, j:j+window_size]
                valid_mask = ~np.isnan(window)
                valid_values = window[valid_mask]
                if valid_values.size > 0:
                    smoothed_matrix[i, j] = np.mean(valid_values)
                else:
                    smoothed_matrix[i, j] = matrix[i, j]

    return smoothed_matrix

def moving_average_2d_with_weight(matrix, weights, window_size=3):
    if weights.shape != matrix.shape:
        raise ValueError("Weights matrix must have the same shape as the input matrix")

    padded_matrix = np.pad(matrix, pad_width=window_size//2, mode='edge')
    padded_weights = np.pad(weights, pad_width=window_size//2, mode='edge')
    smoothed_matrix = np.zeros_like(matrix)

    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if np.isnan(matrix[i, j]):
                # 保持NaN值不变
                smoothed_matrix[i, j] = matrix[i, j]
            else:
                # 计算加权窗口内的平均值，忽略NaN值
                window = padded_matrix[i:i+window_size, j:j+window_size]
                weight_window = padded_weights[i:i+window_size, j:j+window_size]
                valid_mask = ~np.isnan(window)
                valid_values = window[valid_mask]
                valid_weights = weight_window[valid_mask]
                if valid_values.size > 0:
                    smoothed_matrix[i, j] = np.average(valid_values, weights=valid_weights)
                else:
                    smoothed_matrix[i, j] = matrix[i, j]

    return smoothed_matrix

def draw_half_circle_outline(ax, center, radius, linewidth,edgecolor='black',fill=True):
    # 创建一个theta数组，表示角度从-pi/2到pi/2（向右的半圆）
    theta = np.linspace(-np.pi / 2, np.pi / 2, 100)

    # 创建一个半径为radius的半圆
    x = center[0] + radius * np.cos(theta)
    y = center[1] + radius * np.sin(theta)

    # 绘制半圆线条
    if fill:
        ax.fill_betweenx(y, center[0], x, color='white',zorder=10)
    ax.plot(x, y, color=edgecolor, linewidth=linewidth,zorder=11)

def InCircle_xy(point,Center,radius):
    return ((point[0]-Center[0])**2+(point[1]-Center[1])**2)<radius**2

def calFractionInPipe(point,Center,radius,smallMesh,smallMeshSize,smallMeshSideNum):
    inNum=0
    for i in smallMesh:
        inNum+=InCircle_xy([i[0]+point[0]+smallMeshSize/2,i[1]+point[1]+smallMeshSize/2],Center,radius)
    return 1-inNum*1.0/smallMeshSideNum/smallMeshSideNum

def periodicialize(data,boundaryDict_cell,xyz='xyz'):
    if len(data)==len(xyz):
        data=list(data)
    else:
        raise ValueError("Input to function periodicialize is wrong. Please check.")
    checklist=[]
    for i in xyz:
        if i=='x': checklist.append(0)
        elif i=='y': checklist.append(1)
        elif i=='z': checklist.append(2)
    output=[]
    for d,l in zip(data,checklist):
        output.append(d%boundaryDict_cell[l])
    return output
    
class mesh_xz:
    def __init__(self,level,boundaryDict,positionList,smallMeshSideNum=500,smallorlarge=1) -> None:
        self.aabbx=[boundaryDict['aabbmin'][0],boundaryDict['aabbmax'][0]]
        self.aabbz=[boundaryDict['aabbmin'][2],boundaryDict['aabbmax'][2]]
        self.zone1aabbmin=boundaryDict['zonesAABB'][0][0]
        self.zone1aabbmax=boundaryDict['zonesAABB'][0][1]
        self.level=level
        self.meshSize=(boundaryDict['aabbmax'][0]-boundaryDict['aabbmin'][0])/self.level
        self.abx=[self.zone1aabbmin[0],self.zone1aabbmax[0]-0.005]
        self.abz=[positionList[boundaryDict['shearbox_id']][2]-0.07,positionList[boundaryDict['shearbox_id']][2]+0.07]
        if smallorlarge!=0:
            self.thisAabbX=self.abx
            self.thisAabbZ=self.abz
        elif smallorlarge==0:
            self.thisAabbX=self.aabbx
            self.thisAabbZ=self.aabbz
        self.xMesh=np.arange(self.thisAabbX[0],self.thisAabbX[1],self.meshSize)
        self.zMesh=np.arange(self.thisAabbZ[0],self.thisAabbZ[1],self.meshSize)
        self.xMeshNum=len(self.xMesh)
        self.zMeshNum=len(self.zMesh)
        self.meshAabbMin=[self.zone1aabbmin[0],self.zone1aabbmin[1],self.thisAabbZ[0]]
        self.smallMeshSideNum=smallMeshSideNum
        self.smallMeshSize=self.meshSize/self.smallMeshSideNum
        self.smallMesh=[]
        for i in range(self.smallMeshSideNum):
            for j in range(self.smallMeshSideNum):
                self.smallMesh.append([i*self.smallMeshSize,j*self.smallMeshSize])
        self.meshVolume=self.meshSize*self.meshSize*boundaryDict['aabbmax'][1]
        self.space=[[self.meshVolume for j in range(self.xMeshNum)] for i in range(self.zMeshNum)]
        self.zone3aabbmin=boundaryDict['zonesAABB'][2][0]
        self.zone3aabbmax=boundaryDict['zonesAABB'][2][1]
        self.zone3aabbmin_meshed=self.meshilize(self.zone3aabbmin)
        self.zone3aabbmax_meshed=self.meshilize(self.zone3aabbmax)
        self.zone2aabbmin=boundaryDict['zonesAABB'][1][0]
        self.zone2aabbmax=boundaryDict['zonesAABB'][1][1]


    def updateSpace(self,ppCenter_xz,ppR,calTouchedMesh=False):
        self.ppCenter_x=ppCenter_xz[0]
        self.ppCenter_z=ppCenter_xz[1]
        for i in range(self.zMeshNum):
            for j in range(self.xMeshNum):
                Num=int(InCircle_xy([self.xMesh[j],self.zMesh[i]],ppCenter_xz,ppR))+int(InCircle_xy([self.xMesh[j]+self.meshSize,self.zMesh[i]],ppCenter_xz,ppR))+int(InCircle_xy([self.xMesh[j],self.zMesh[i]+self.meshSize],ppCenter_xz,ppR))+int(InCircle_xy([self.xMesh[j]+self.meshSize,self.zMesh[i]+self.meshSize],ppCenter_xz,ppR))
                if Num==4:
                    self.space[i][j]=np.nan
                elif Num==0:
                    self.space[i][j]=self.meshVolume

                else:
                    if calTouchedMesh: self.space[i][j]=calFractionInPipe([self.xMesh[j],self.zMesh[i]],ppCenter_xz,ppR,self.smallMesh,self.smallMeshSize,self.smallMeshSideNum)*self.meshVolume
                    else: self.space[i][j]=np.nan
    def meshList(self,initialValue):
        return [[initialValue for j in range(self.xMeshNum)]for i in range(self.zMeshNum)]
    def meshilize(self,data):
        if len(data)!=3:
            raise ValueError("Input to method mesh_xz.meshilize is wrong. Please check.")
        return [(data[i]-self.meshAabbMin[i])/self.meshSize for i in range(3)]
    def meshilize_length(self,data):
        return data/self.meshSize

def principal_stress_and_direction(sigma_x, sigma_y, tau_xy):
    # 计算平均应力
    sigma_avg = (sigma_x + sigma_y) / 2
    
    # 计算莫尔圆的半径
    R = math.sqrt(((sigma_x - sigma_y) / 2)**2 + tau_xy**2)
    
    # 计算主应力
    sigma_1 = sigma_avg + R
    sigma_2 = sigma_avg - R
    
    # 计算主应力方向的角度（弧度）
    theta_p = 0.5 * math.atan2(2 * tau_xy, sigma_x - sigma_y)
    
    # 将角度转换为度并调整为向上为0度，顺时针为正
    theta_p_deg = math.degrees(theta_p)
    
    return sigma_1, sigma_2, theta_p_deg

# print(principal_stress_and_direction(0,0, 1))