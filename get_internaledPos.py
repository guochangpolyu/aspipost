"""
Please run python3 cal_importantParticles.py before running
Y4.7.2 -j4 get_internaledPos.py
"""
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import numpy as np
from myCommonFunction import *


internaledDict=dict()
path=internalPath[0]
print(f'___Yade file: {path} is being loaded.')
O.load(abspath+'/'+path)

internaledPositionList=[list(b.state.pos) for b in O.bodies]

saver(internaledPositionList,internaledPositionListPath)
print(f'internaledPositionList is saved to {postPath}.')
