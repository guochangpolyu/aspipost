"""
Please run Y4.7.2 -j4 get_pressureMatrixZone5.py before running
python3 draw_pressureMatrixEarth.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from myCommonFunction import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
ppPosDict=reader(ppPosDictPath)

### define and create path for saving
thisSavePath=pressureMatrix_earth_path
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)

### define and get path for reading analysed data
thisReadName=pressureMatrixZone5Path
dataName='pressureMatrixZone5'
f = os.listdir(thisReadName)
f= [i for i in f if dataName+'.data' in i]
f.sort()

### initialize a mesh
thisMesh=mesh_xz(100,boundaryDict,positionList,smallorlarge=0)

### common plt setting
config = {
    "font.family":'Arial',
    "font.size": 16,
    }
Earth1Pos=[ppPosDict['000'][0]+1.5*OutDiameter,ppPosDict['000'][1],ppPosDict['000'][2]+1*OutDiameter]
Earth2Pos=[ppPosDict['000'][0],ppPosDict['000'][1],ppPosDict['000'][2]+1*OutDiameter]
Earth3Pos=[ppPosDict['000'][0]+1.5*OutDiameter,ppPosDict['000'][1],ppPosDict['000'][2]]
Earth4Pos=[ppPosDict['000'][0],ppPosDict['000'][1],ppPosDict['000'][2]-1*OutDiameter]
Earth5Pos=[ppPosDict['000'][0]+1.5*OutDiameter,ppPosDict['000'][1],ppPosDict['000'][2]-1*OutDiameter]

Earth1Pos_meshed=thisMesh.meshilize(Earth1Pos)
Earth2Pos_meshed=thisMesh.meshilize(Earth2Pos)
Earth3Pos_meshed=thisMesh.meshilize(Earth3Pos)
Earth4Pos_meshed=thisMesh.meshilize(Earth4Pos)
Earth5Pos_meshed=thisMesh.meshilize(Earth5Pos)
Earth1Pos_intxz=[int(Earth1Pos_meshed[0]),int(Earth1Pos_meshed[2])]
Earth2Pos_intxz=[int(Earth2Pos_meshed[0]),int(Earth2Pos_meshed[2])]
Earth3Pos_intxz=[int(Earth3Pos_meshed[0]),int(Earth3Pos_meshed[2])]
Earth4Pos_intxz=[int(Earth4Pos_meshed[0]),int(Earth4Pos_meshed[2])]
Earth5Pos_intxz=[int(Earth5Pos_meshed[0]),int(Earth5Pos_meshed[2])]

Earths1=[]
Earths2=[]
Earths3=[]
Earths4=[]
Earths5=[]
ppDisplacementList=[]
gap=10
n=0
for p in f:
    if (n%gap==0 and n<=200):
        pathHead=p.replace(dataName+'.data','')
        ppPos=ppPosDict[pathHead]
        displacement=int(pathHead)
        ppDisplacement=displacement/10000.0
        ppDisplacementList.append(ppDisplacement)
        print(f'Now analyse the condition of axial displacement is {displacement}')

        thisMesh.updateSpace([ppPos[0],ppPos[2]],ppR,calTouchedMesh=1)
        pressures=thisMesh.meshList(0.0)

        pressureMatrixList=reader(thisReadName+p)
        for contactPoint,pressureMatrix in pressureMatrixList:
            c=thisMesh.meshilize(periodicialize(contactPoint,boundaryDict_cell=boundaryDict['cell'],xyz='xyz'))
            if (0<=c[0]<thisMesh.xMeshNum) and (0<=c[2]<thisMesh.zMeshNum):
                i=int(c[2])
                j=int(c[0])
                pressures[i][j]+=pressureMatrix[2][2]
        pressures=[[np.nan if thisMesh.space[i][j]==np.nan or thisMesh.space[i][j]==0.0 else -pressures[i][j]/thisMesh.space[i][j]/1000 for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]
        window_size=8
        pressures=moving_average_2d_with_weight(np.array(pressures),np.array(thisMesh.space), window_size=window_size)
        Earths1.append(pressures[Earth1Pos_intxz[1]][Earth1Pos_intxz[0]])
        Earths2.append(pressures[Earth2Pos_intxz[1]][Earth2Pos_intxz[0]])
        Earths3.append(pressures[Earth3Pos_intxz[1]][Earth3Pos_intxz[0]])
        Earths4.append(pressures[Earth4Pos_intxz[1]][Earth4Pos_intxz[0]])
        Earths5.append(pressures[Earth5Pos_intxz[1]][Earth5Pos_intxz[0]])

    n+=1
fig1, ax1=plt.subplots(figsize=(10,10))
ax1.plot(ppDisplacementList, Earths1,label='Pos #1')
ax1.plot(ppDisplacementList, Earths2,label='Pos #2')
ax1.plot(ppDisplacementList, Earths3,label='Pos #3')
ax1.plot(ppDisplacementList, Earths4,label='Pos #4')
ax1.plot(ppDisplacementList, Earths5,label='Pos #5')
plt.legend(loc=0)
plt.ylim(0,70)
plt.xlim(0,0.02)
plt.tight_layout()

figName=thisSavePath+'earth_development'
fig1.savefig(figName+'.png', dpi=300)
print("save figure: "+figName+'.png')
fig1.savefig(thisSavePathsvg+'earth_development'+'.svg')
plt.close()