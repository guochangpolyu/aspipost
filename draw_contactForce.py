"""
Please run python3 cal_contactForce.py before running
python3 draw_contactForce.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from myCommonFunction import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
ppPosDict=reader(ppPosDictPath)

### define and create path for saving
thisSavePath=contactForceFigurePath
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)

### define and get path for reading analysed data
thisReadName=contactForceDataPath
checkName='ForceChain.data'
fileName = os.listdir(thisReadName)
f = [i for i in fileName if checkName in i]
f.sort()
checkName2='MaxNormalForces.data'
maxNormalForces=reader(thisReadName+checkName2)
maxNormalForceInHistory=max(maxNormalForces)
### initialize a mesh
thisMesh=mesh_xz(200,boundaryDict,positionList)

### common plt setting
config = {
    "font.family":'Arial',
    "font.size": 16,
    }

gap=10000
n=0
num=10000
for p in f:
    if (n%gap==0):
        pathHead=p.replace(checkName,'')
        ppPos=ppPosDict[pathHead]
        displacement=int(p.replace(checkName,''))
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')

        rcParams.update(config)
        fig1, ax1 = plt.subplots(figsize=(7,10))
        nn=0

        forceChainList=reader(thisReadName+p)
        for pos1,pos2,normalForce in forceChainList:
            nn+=1
            if nn>num: continue
            a=ax1.plot(pos1,pos2,linewidth=normalForce, color=normalForce
                    #    color=str(1-normalForce/maxNormalForceInHistory)
                    # cmap='BuGn',
                    # angles='xy', # determine angle based on 'xy': x, y to x+dx,y+dy; 'uv': 0,0 to dx,dy
                    # scale_units='xy',
                    # scale=0.2, # scale of vector length, 0.1 means 10 times, 10 means 1/10, 1is the real conditon
                    # width=0.001, # size of vectors
                    )


        # a=ax1.imshow(pressures,  interpolation='bilinear')

        # draw_half_circle_outline(ax1, [ppPos[0],ppPos[2]], ppR, 1,edgecolor='black',fill=0)

        # cbar=fig1.colorbar(a, ax=ax1,extend='max')
        # cbar.mappable.set_clim(0,math.ceil((simulationPressure*2)/10.0)*10)

        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        # plt.xticks([])
        # plt.yticks([])

        ax1.set(xlim=thisMesh.abx, ylim=thisMesh.abz)

        ax1.set_aspect('equal')
        plt.tight_layout()

        figName=thisSavePath+pathHead
        figName=pathHead
        fig1.savefig(figName+'.png', dpi=300)
        print("save figure: "+figName+'.png')
        fig1.savefig(thisSavePathsvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'.svg')
        plt.close()

    n+=1
