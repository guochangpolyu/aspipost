"""
Please run Y4.7.2 -j4 get_contactForces.py before running
python3 cal_contactForce.py
"""
##########################################  
import sys
sys.path.append('')
from myCommonSavers import *
from setting import *

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
importantParticlesID=reader(importantParticlesIDPath)

### define and create path for saving
thisSavePath=contactForceDataPath
checkPath(thisSavePath)
print(f'Data of contact Force are saved in '+thisSavePath)

### define and get path for reading analysed data
thisReadName=contactForcePath
f = os.listdir(thisReadName)
checkName='contactForce.data'
f= [i for i in f if checkName in i]
f.sort()

importantParticlesIDinZone1=[id for id in importantParticlesID if id in boundaryDict['zone1IDs']]
boundaryList=[boundaryDict['wall_id_X0'],boundaryDict['wall_id_X1'],boundaryDict['wall_id_Z0'],boundaryDict['wall_id_Z1']]

gap=1
n=0
maxNormalForces=[]
for p in f:
    if (n%gap==0):
        contactForceList=reader(thisReadName+p)
        displacement=int(p.replace(checkName,''))
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')
        forceChainList=[]
        maxNormalForce=0.0
        for i in contactForceList:
            if (not (i[0] in boundaryList)) and (not (i[1] in boundaryList)) and ((i[0] in importantParticlesIDinZone1) or (i[1] in importantParticlesIDinZone1)):
                forceChainList.append([[positionList[i[0]][0],positionList[i[1]][0]],[positionList[i[0]][2],positionList[i[1]][2]],i[4]])
                maxNormalForce=max(maxNormalForce,i[4])
        maxNormalForces.append(maxNormalForce)
        saver(forceChainList,thisSavePath+str(int(float(ppDisplacement)*10000)).zfill(3)+'ForceChain.data')
        print(f'ForceChain data is saved to {thisSavePath}.')
    n+=1

saver(maxNormalForces,thisSavePath+'MaxNormalForces.data')
print(f'Max Normal Forces data is saved to {thisSavePath}.')