import pickle,sys,os
def saver(sth,fileName:str):
    """save sth to the file with a name of fileName, you'd better give the filename with an end of .data
    """
    f=open(fileName,'wb')
    pickle.dump(sth,f)
    f.close()


def reader(fileName:str):
    """read sth from the file with a name of fileName

    Returns:
        sth: the thing saved in fileName
    """
    f = open(fileName,'rb')
    sth =pickle.load(f)
    f.close()
    return sth

def orginzeList(key_list:list,*items:str,position:int=0):
    """key_list is a list of str. the list of *items will be move to the position of position.
    Args:
        position (int): target position. Defaults to 0.
    """
    ls=list(items)
    ls.reverse()
    for i in ls:
        key_list.remove(i)
        key_list.insert(position,i)



#### log ####
class Logger(object):
    def __init__(self, filename, stream):
        self.terminal = stream
        # self.error = sys.stderr
        self.log = open(filename, "a")  # w for writting only. replacing it as a will be 'add'

    def write(self, message):
        self.terminal.write(message)
        # self.error.write(message)
        self.log.write(message)

    def flush(self):
        self.terminal.flush()
        # self.error.flush()
        self.log.flush()

    def readline(self):
        return self.terminal.readline()
def creatLog(path:str):

    global Sstdout, Sstderr, Sstdin
    Sstdout = Logger(path + 'log.log', sys.stdout)
    Sstderr = Logger(path + 'log.log', sys.stderr)
    Sstdin = Logger(path + 'log.log', sys.stdin)
    print('log in'+ path + 'log.log')
def beginLog():
    # print(1)
    sys.stdout = Sstdout
    sys.stderr = Sstderr
    sys.stdin = Sstdin
def flushLog():
    Sstdout.flush()
    Sstderr.flush()
    Sstdin.flush()
#### log ####


def checkPath(path:str):
    """check the path(folder) is existing or not. if not, the folder will be created
    """
    if path!='':
            if not os.path.exists(path):
                os.makedirs(path)


def writeDict2csv(dict:dict,headers:list,fileName:str,mode:str='w'):
    with open(fileName,mode) as f:
        item = [str(i) for i in headers]
        f.write(','.join(item) + "\n") 
        for item in zip(*dict.values()): 
            item = [str(i) for i in item] 
            f.write(','.join(item) + "\n")
def writeList2csv(list:list,fileName:str,mode:str='w'):
    with open(fileName,mode) as f:
        for item in list: 
            item = [str(i) for i in item] 
            f.write(','.join(item) + "\n")

def readPathFile(filename:str)->list:
    with open(filename+'.path','r') as f:
        lines=f.readlines()
    return [line.rstrip() for line in lines]