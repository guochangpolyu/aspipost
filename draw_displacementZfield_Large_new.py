"""
Please run python3 cal_importantParticles.py before running
python3 draw_displacementZfield_Large_new.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.colors as colors
from matplotlib.colors import ListedColormap
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
ppPosDict=reader(ppPosDictPath)
zone5Path=postPath+'/displacement/displacementZ/Zone5/'
zone5PathSvg=zone5Path+'svg/'
checkPath(zone5Path)
checkPath(zone5PathSvg)
print(f'Figures of zone 5 are saved in '+zone5Path)

f = os.listdir(displacementPath)
f= [i for i in f if 'dis.data' in i]
f.sort()

aabbx=[boundaryDict['aabbmin'][0],boundaryDict['aabbmax'][0]]
aabbz=[boundaryDict['aabbmin'][2],boundaryDict['aabbmax'][2]]
zone1aabbmin=boundaryDict['zonesAABB'][0][0]
zone1aabbmax=boundaryDict['zonesAABB'][0][1]
abx=[zone1aabbmin[0],zone1aabbmax[0]]
# abz=[zone1aabbmin[2],zone1aabbmax[2]]
abz=[positionList[boundaryDict['shearbox_id']][2]-0.075,positionList[boundaryDict['shearbox_id']][2]+0.075]
sizeList=reader(sizeListPath)
importantParticlesID=reader(importantParticlesIDPath)
gap=60
n=0

thisMesh=mesh_xz(200,boundaryDict,positionList,smallorlarge=0)


for p in f:
    if (n%gap==0) and n !=0:
        x0 = []
        z0 = []
        y0 =  []
        size = []
        for id in importantParticlesID:
            if positionList[id][1]%boundaryDict['aabbmax'][1] < boundaryDict['aabbmax'][1]*1:
                # if thisMesh.zone1aabbmin[1]<=positionList[id][1] <=thisMesh.zone1aabbmin[1]+(thisMesh.zone1aabbmax[1]-thisMesh.zone1aabbmin[1])*0.25:
                if thisMesh.zone3aabbmin[0]-0.0001<=positionList[id][0]<=thisMesh.zone3aabbmax[0]+0.005 and thisMesh.zone3aabbmin[2]-0.0074-0.005<=positionList[id][2]<=thisMesh.zone3aabbmax[2]-0.0074+0.005:
                    x0.append(positionList[id][0])
                    z0.append(positionList[id][2])
                    y0.append(positionList[id][1])
                    size.append((sizeList[id])**2.5/60.0*2*12)
        disList=reader(displacementPath+p)
        displacement=int(p.replace('dis.data',''))
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')
        dz = []
        for id in importantParticlesID:
            if thisMesh.zone3aabbmin[0]-0.0001<=positionList[id][0]<=thisMesh.zone3aabbmax[0]+0.005 and thisMesh.zone3aabbmin[2]-0.0074-0.005<=positionList[id][2]<=thisMesh.zone3aabbmax[2]-0.0074+0.005:
                dz.append(disList[id][2])

        config = {
            "font.family":'Arial',
            "font.size": 16,
        }
        rcParams.update(config)

        fig1, ax1 = plt.subplots(figsize=(7,10))
        rdz=[i*1000 for i in dz]

        # 调整x0和z0在某个范围之内的颗粒，如果其rdz小于某个值就将这个值改为0
        x_range = [abx[0],abx[1]-0.01]
        z_range = [abz[0]+0.01,abz[1]-0.01]
        threshold = -0.2

        for i in range(len(x0)):
            if not (x_range[0] <= x0[i] <= x_range[1] and z_range[0] <= z0[i] <= z_range[1]):
                if rdz[i] < threshold:
                    rdz[i] *= 0.04


        maxx=0.4
        minn=-0.4
        from scipy.spatial import KDTree
        # 使用KDTree找到每个点最近的5个点并计算平均值，剔除与其他数字差别明显的数字
        points = np.column_stack((x0, z0))
        tree = KDTree(points)
        rdz_avg = []

        for point in points:
            distances, indices = tree.query(point, k=31)  # k=6 because the point itself is included
            values = [rdz[i] for i in indices]
            mean_value = np.mean(values)
            std_dev = np.std(values)
            filtered_values = [v for v in values if abs(v - mean_value) <= std_dev]  # 剔除与其他数字差别明显的数字
            avg_value = np.mean(filtered_values)
            rdz_avg.append(avg_value)
        rdz=rdz_avg
        sorted_indices = np.argsort(rdz)
        x0 = np.array(x0)[sorted_indices]
        y0 = np.array(y0)[sorted_indices]
        z0 = np.array(z0)[sorted_indices]
        size = np.array(size)[sorted_indices]
        rdz = np.array(rdz)[sorted_indices]
        x00=[]
        y00 = []
        z00 = []
        size0 =[]
        rdz0 = []
        for x,y,z,s,d in zip(x0,y0,z0,size,rdz):
            if thisMesh.zone1aabbmin[1]<=y <=thisMesh.zone1aabbmin[1]+(thisMesh.zone1aabbmax[1]-thisMesh.zone1aabbmin[1])*0.25:
                x00.append(x)
                y00.append(y)
                z00.append(z)
                size0.append(s)
                rdz0.append(d)

        cmap = plt.get_cmap('seismic')
        newcolors=cmap(np.linspace(0, 1, 41))
        cmap = ListedColormap(newcolors) 
        norm = plt.Normalize(minn,maxx)
        a = ax1.scatter(x00, z00, c=rdz0, norm=norm, cmap=cmap, s=size0, alpha=1, edgecolors='none')
        cbar = fig1.colorbar(a, ax=ax1, extend='both', ticks=np.linspace(minn, maxx, 11))
        cbar.mappable.set_clim(minn, maxx)
        cbar.cmap.set_under(cmap(0))
        cbar.cmap.set_over(cmap(cmap.N - 1))
        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        plt.xticks([])
        plt.yticks([])

        ax1.set(xlim=aabbx, ylim=aabbz)
        ax1.set(xlim=[thisMesh.zone3aabbmin[0],thisMesh.zone3aabbmax[0]], ylim=[thisMesh.zone3aabbmin[2]-0.0074,thisMesh.zone3aabbmax[2]-0.0074])
        ax1.set_aspect('equal')
        plt.tight_layout()
        plt.savefig(zone5Path+str(int(float(ppDisplacement)*10000)).zfill(3)+'.png', dpi=300)
        print("save figure: "+zone5Path+str(int(float(ppDisplacement)*10000)).zfill(3)+'.png')
        plt.savefig(zone5PathSvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'.svg')
        plt. close()
        # pathHead=p.replace('dis.data','')
        # ppPos=ppPosDict[pathHead]
        # thisMesh.updateSpace([ppPos[0],ppPos[2]],ppR,calTouchedMesh=1)
        # disY=thisMesh.meshList(0.0)
        # disN=thisMesh.meshList(0.0)
        # for x, z, dz in zip(x0, z0, rdz):
        #     c=thisMesh.meshilize(periodicialize((x,0,z),boundaryDict_cell=boundaryDict['cell'],xyz='xyz'))
        #     if (0<=c[0]<thisMesh.xMeshNum) and (0<=c[2]<thisMesh.zMeshNum):
        #         i=int(c[2])
        #         j=int(c[0])
        #         disY[i][j]+=dz
        #         disN[i][j]+=1
        # # disY=[[np.nan if disN[i][j]==np.nan or disN[i][j]==0 else disY[i][j]*1.0/disN[i][j] for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]
        # disY=[[0.0 if disN[i][j]==np.nan or disN[i][j]==0 else disY[i][j]*1.0/disN[i][j] for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]

        # window_size=3
        # disY=moving_average_2d_with_weight(np.array(disY),np.array(thisMesh.space), window_size=window_size)

        # # disY=[[np.nan if thisMesh.space[i][j]==np.nan or thisMesh.space[i][j]==0.0 else disY[i][j] for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]
        # rcParams.update(config)
        # fig1, ax1 = plt.subplots(figsize=(7,10))

        # cmap = plt.get_cmap('seismic')
        # newcolors=cmap(np.linspace(0, 1, 41))
        # cmap = ListedColormap(newcolors) 
        # # norm = plt.Normalize(minn,maxx)
        # # a = ax1.scatter(x0, z0, c=rdz, norm=norm, cmap=cmap, s=size, alpha=1, edgecolors='none')
        # a=ax1.imshow(disY,  interpolation='bilinear',cmap=cmap)
        # cbar = fig1.colorbar(a, ax=ax1, extend='both', ticks=np.linspace(minn, maxx, 11))
        # cbar.mappable.set_clim(minn, maxx)
        # cbar.cmap.set_under(cmap(0))
        # cbar.cmap.set_over(cmap(cmap.N - 1))

        # ppR_meshed=thisMesh.meshilize_length(ppR)
        # ppC_meshed=thisMesh.meshilize(ppPos)
        # draw_half_circle_outline(ax1, [ppC_meshed[0]-0.5,ppC_meshed[2]-0.5], ppR_meshed, 8,edgecolor='black',fill=1)

        # roughness=''
        # if 'R' in targetSimulation: roughness='1.01'
        # elif 'S' in targetSimulation: roughness='0.04'

        # ax1.set_title(r'$\mathit{R}_{n}=$' + roughness + ', ${u}$ = ' + '%.1f' % (float(ppDisplacement) * 1000) + " mm", fontname='Arial', fontsize=30)

        # plt.xticks([])
        # plt.yticks([])

        # ax1.set(xlim=[thisMesh.zone3aabbmin_meshed[0],thisMesh.zone3aabbmax_meshed[0]], ylim=[thisMesh.zone3aabbmin_meshed[2],thisMesh.zone3aabbmax_meshed[2]])
        # ax1.set_aspect('equal')
        # plt.tight_layout()

        # plt.savefig(zone5Path+str(int(float(ppDisplacement)*10000)).zfill(3)+'new.png', dpi=300)
        # print("save figure: "+zone5Path+str(int(float(ppDisplacement)*10000)).zfill(3)+'new.png')
        # plt.savefig(zone5PathSvg+str(int(float(ppDisplacement)*10000)).zfill(3)+'new.svg')
        # plt. close()
    n+=1
