"""
Please run python3 cal_importantParticles.py before running
python3 draw_displacementYfield_Large.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.colors as colors
from matplotlib.colors import ListedColormap

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
importantParticlesID=reader(importantParticlesIDPath)
sizeList=reader(sizeListPath)

### define and create path for saving
thisSavePath=displacement_Y_zone5Path
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)
print(f'Figures of zone 5 are saved in '+thisSavePath)

### define and get path for reading analysed data
thisReadName=displacementPath
dataName='dis'
f = os.listdir(thisReadName)
f= [i for i in f if dataName+'.data' in i]
f.sort()

### initialize a mesh
thisMesh=mesh_xz(100,boundaryDict,positionList,smallorlarge=0)

gap=200
n=0

x0 = []
z0 = []
size = []
thisIds = []
for id in importantParticlesID:
    if thisMesh.zone1aabbmin[0]-0.001<=positionList[id][0] <=thisMesh.zone1aabbmax[0]+0.001:
        if thisMesh.zone1aabbmin[2]-0.001+0.005<=positionList[id][2] <=thisMesh.zone1aabbmax[2]-0.01+0.001+0.005:
            if thisMesh.zone1aabbmin[1]<=positionList[id][1] <=thisMesh.zone1aabbmin[1]+(thisMesh.zone1aabbmax[1]-thisMesh.zone1aabbmin[1])*0.25:
                thisIds.append(id)
for i in thisIds:
    x0.append(positionList[i][0])
    z0.append(positionList[i][2])
    size.append((sizeList[i])**2.5/60.0*24*4.1)


for p in f:
    if (n%gap==0):
        pathHead=p.replace(dataName+'.data','')
        disList=reader(displacementPath+p)
        displacement=int(pathHead)
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')
        dy = []
        for i in thisIds: 
            dy.append(disList[i][1]*1000)

        # 将列表组合成一个包含元组的列表
        combined_lists = list(zip(dy, x0, z0,size))

        # 按照第一个列表的数字从大到小排序
        sorted_combined_lists = sorted(combined_lists, key=lambda x: x[0], reverse=0,)

        # 将排序后的组合列表解压回单独的列表
        sorted_dy, sorted_x0, sorted_z0,sorted_size = zip(*sorted_combined_lists)
        config = {
            "font.family":'Arial',
            "font.size": 16,
        }
        rcParams.update(config)
        fig1, ax1 = plt.subplots(figsize=(7.68,10))
        tick=np.linspace(0,200*0.1,11)
        # cmap=plt.get_cmap('viridis')
        # newcolors=cmap(np.linspace(0, 1, 256))
        # newcmap = ListedColormap(newcolors) 
        # tick=[0, 2, 4, 6, 8,10,12,14,16,18,20]
        # factor=1
        # if (targetSimulation=='S34'): factor=0.3
        # tick1=[i*factor for i in tick]
        bounds = np.linspace(0, max(tick), 21)
        norm = colors.BoundaryNorm(boundaries=bounds, ncolors=160)
        a=ax1.scatter(sorted_x0, sorted_z0, c=sorted_dy, 
                    #   norm=norm,
                    # cmap=newcmap,
                      s=sorted_size,alpha=1,edgecolors='none')
        cbar=fig1.colorbar(a, ax=ax1,ticks=tick)
        cbar.mappable.set_clim(0,max(tick))
        
        cbar.ax.set_yticklabels(['%.1f' %i for i in tick])
        cbar.set_label('Axial displacement: mm',rotation=270,fontsize=24, labelpad=20)
        # ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        plt.xticks([])
        plt.yticks([])

        ax1.set(xlim=[thisMesh.zone1aabbmin[0],thisMesh.zone1aabbmax[0]], ylim=[thisMesh.zone1aabbmin[2]+0.005,thisMesh.zone1aabbmax[2]-0.01+0.005])
        ax1.set_aspect('equal')
        plt.tight_layout()
        figName=thisSavePath+pathHead
        plt.savefig(figName+'.png', dpi=300)
        print("save figure: "+figName+'.png')
        plt.savefig(thisSavePathsvg+pathHead+'.svg')
        plt.savefig(thisSavePathsvg+pathHead+'.svgz')
        plt.close()
    n+=1
