"""
Please run Y4.7.2 -j4 get_boundaryInfo.py before running
python3 cal_importantParticles.py
"""
##########################################  
import sys
sys.path.append('')
from myCommonSavers import *
from setting import *
boundaryDict=reader(postPath+'/boundaryDict.data')


f = os.listdir(displacementPath)
f= [i for i in f if 'dis.data' in i]
f.sort()

importantParticlesID=[]
p=f[-1]
print('Check important particels using '+displacementPath+p)
print('Particles with id of ', end='')
disList=reader(displacementPath+p)
displacement=int(p.replace('dis.data',''))
ppDisplacement=displacement/10000.0
paID = boundaryDict['parIDs']
n=0
for id in paID:
    if(disList[id][0]**2+disList[id][1]**2+disList[id][2]**2)**0.5<ppDisplacement*1.2:
        importantParticlesID.append(id)
    else:
        print(id,end=', ')
        n+=1

print(f'will not be analyzed with {n} particles in total.')

saver(importantParticlesID,importantParticlesIDPath)
print(f'importantParticlesID is saved to {postPath}.')