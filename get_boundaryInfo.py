'''
Y4.7.2 -j4 get_boundaryInfo.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import numpy as np
from myCommonFunction import *


boundaryDict=dict()
path=PPath[0]
print(f'___Yade file: {path} is being loaded.')
O.load(abspath+'/'+path)
dsb=DirectShearBox()

for e in O.engines:
    if (isinstance(e,DirectShearBox6D) or isinstance(e,DirectShearBox)):
        dsb=e
        break
boundaryDict['parIDs']=[b.id for b in O.bodies if (isinstance(b.shape,Sphere) and (not b.isClumpMember))]
boundaryDict['wall_id_X0']=dsb.wall_ids_X0[0]
boundaryDict['wall_id_X1']=dsb.wall_ids_X1[0]
boundaryDict['wall_id_Z0']=dsb.wall_id_Z0
boundaryDict['wall_id_Z1']=dsb.wall_id_Z1
boundaryDict['shearbox_id']=dsb.shearbox_id
boundaryDict['aabbmin']=[(O.bodies[dsb.wall_ids_X0[0]].state.pos+O.bodies[dsb.wall_ids_X0[0]].shape.extents)[0],0,(O.bodies[dsb.wall_id_Z0].state.pos+O.bodies[dsb.wall_id_Z0].shape.extents)[2]]
boundaryDict['aabbmax']=[(O.bodies[dsb.wall_ids_X1[0]].state.pos-O.bodies[dsb.wall_ids_X1[0]].shape.extents)[0],O.cell.hSize[1][1],(O.bodies[dsb.wall_id_Z1].state.pos-O.bodies[dsb.wall_id_Z1].shape.extents)[2]]
boundaryDict['d']=[2*O.bodies[id].shape.radius for id in boundaryDict['parIDs']]
boundaryDict['d50']=(max(boundaryDict['d'])+min(boundaryDict['d']))/2.0
boundaryDict['shearbox_members']= [b.id for b in O.bodies if (isinstance(b.shape,Sphere) and (b.isClumpMember))]
positionList=[list(b.state.pos) for b in O.bodies]
boundaryDict['cell']=[O.cell.hSize[0][0],O.cell.hSize[1][1],O.cell.hSize[2][2]]

anglePosList=[]
posShearbox=O.bodies[boundaryDict['shearbox_id']].state.pos

posShearbox[0]=WidthX0-targetWidthX / 2.0
for b in O.bodies:
    relativePos=b.state.pos-posShearbox
    relativePos[1]=b.state.pos[1]
    anglePos=angleVector(relativePos)
    anglePosList.append(list(anglePos))

path=internalPath[0]

targetLengthY=O.cell.size[1]
spAabbhalf=[[WidthX/2-targetWidthX / 2.0,0,boundaryGap+thickness],[WidthX-(thickness*1+boundaryGap)-targetWidthX / 2.0,targetLengthY,HeightZ1-thickness*0.5]]
spAabbhalf1=[[WidthX/2-targetWidthX / 2.0,0,HeightZ0-0.075],[WidthX/2+0.075-targetWidthX / 2.0,targetLengthY,HeightZ0+0.075]]
spAabbhalf2=[[WidthX/2-targetWidthX / 2.0,0,HeightZ0-0.1],[WidthX/2+0.1-targetWidthX / 2.0,targetLengthY,HeightZ0+0.1]]
spAabbhalf3=[[WidthX/2-targetWidthX / 2.0,0,HeightZ0-0.15],[WidthX/2+0.15-targetWidthX / 2.0,targetLengthY,HeightZ0+0.15]]
spAabbhalf4=[[WidthX/2-targetWidthX / 2.0,0,HeightZ0-0.2],[WidthX/2+0.2-targetWidthX / 2.0,targetLengthY,HeightZ0+0.2]]
spAabbhalf5=spAabbhalf
boundaryDict['zonesAABB']=[spAabbhalf1,spAabbhalf2,spAabbhalf3,spAabbhalf4,spAabbhalf5]
# WidthX0-targetWidthX / 2.0
print(f'___Yade file: {path} is being loaded.')
O.load(abspath+'/'+path)
boundaryDict['zone1IDs']=[]
boundaryDict['zone2IDs']=[]
boundaryDict['zone3IDs']=[]
boundaryDict['zone4IDs']=[]
boundaryDict['zone5IDs']=[]
sizeList=[]
for b in O.bodies:
    if (isinstance(b.shape,Sphere) and (not b.isClumpMember)):
        if ptInAABB(b.state.refPos,spAabbhalf1[0],spAabbhalf1[1]):
            boundaryDict['zone1IDs'].append(b.id)
            sizeList.append(4.0)
        elif ptInAABB(b.state.refPos,spAabbhalf2[0],spAabbhalf2[1]):
            boundaryDict['zone2IDs'].append(b.id)
            sizeList.append(6.0)
        elif ptInAABB(b.state.refPos,spAabbhalf3[0],spAabbhalf3[1]):
            boundaryDict['zone3IDs'].append(b.id)
            sizeList.append(9.0)
        elif ptInAABB(b.state.refPos,spAabbhalf4[0],spAabbhalf4[1]):
            boundaryDict['zone4IDs'].append(b.id)
            sizeList.append(13.5)
        elif ptInAABB(b.state.refPos,spAabbhalf5[0],spAabbhalf5[1]):
            boundaryDict['zone5IDs'].append(b.id)
            sizeList.append(20.25)
        else:
            sizeList.append(0)
    else:
        sizeList.append(0)

saver(boundaryDict,boundaryDictPath)
print(f'boundaryDict is saved to {postPath}, with keys of {list(boundaryDict.keys())}')

saver(positionList,positionListPath)
print(f'positionList is saved to {postPath}.')

saver(anglePosList,postPath+'/anglePosList.data')
print(f'anglePosList is saved to {postPath}.')

saver(sizeList,sizeListPath)
print(f'sizeList is saved to {postPath}.')