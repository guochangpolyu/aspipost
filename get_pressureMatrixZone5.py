'''
get_ file is used for get raw data from Yade
please run Y4.7.2 -j4 get_boundaryInfo.py and python3 cal_importantParticles.py before this code
Y4.7.2 -j4 get_pressureMatrixZone5.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import numpy as np
boundaryDict=reader(boundaryDictPath)

thisSavePath=pressureMatrixZone5Path
checkPath(thisSavePath)
dataName='pressureMatrixZone5'
print(f'_ {dataName} data will be saved in {thisSavePath}.')
importantParticlesID=reader(importantParticlesIDPath)

n=0
gap=1
cover=False
for path in PPath:
    pathHead=path[path.find('Loadingn')+len('Loadingn'):path.find('Y')].zfill(3)
    if (n%gap==0): # set path gap
        savePath=thisSavePath+pathHead+dataName+'.data'
        if cover or (not os.path.exists(savePath)):
            print(f'___Yade file: {path} is being loaded.')
            O.load(abspath+'/'+path)

            pressureMatrixZone5List=[]
            for i in O.interactions:
                if i.isReal and i.isActive:
                    # if (i.id1 in importantParticlesIDinZone1) or (i.id2 in importantParticlesIDinZone1):
                    if (i.id1 in importantParticlesID) or (i.id2 in importantParticlesID):
                        pressureMatrixZone5=(i.phys.normalForce+i.phys.shearForce).outer(O.bodies[i.id1].state.pos-O.bodies[i.id2].state.pos)
                        pressureMatrixZone5List.append([list(i.geom.contactPoint),[list(i) for i in list(pressureMatrixZone5)]])
            saver(pressureMatrixZone5List,savePath)
            print(f'______ {dataName} List is saved to {savePath}.')
    n+=1



