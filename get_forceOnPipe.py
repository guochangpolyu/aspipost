'''
get_ file is used for get raw data from Yade
please run Y4.7.2 -j4 get_boundaryInfo.py before this code
Y4.7.2 -j4 get_forceOnPipe.py
'''
import sys
sys.path.append('')
from setting import *
from myCommonSavers import *
import numpy as np
boundaryDict=reader(boundaryDictPath)
forceOnPipePath=postPath+'/forceOnPipe/rawData/'
checkPath(forceOnPipePath)
print(f'_forceOnPipe data will be saved in {forceOnPipePath}.')
anglePosList=reader(postPath+'/anglePosList.data')

n=0
gap=1
cover=False
for path in PPath:
    pathHead=path[path.find('Loadingn')+len('Loadingn'):path.find('Y')].zfill(3)
    if (n%gap==0): # set path gap
        savePath=forceOnPipePath+pathHead+'forceOnPipe.data'
        print()
        print(f'___Yade file: {path} is checking.')
        if cover or (not os.path.exists(savePath)):
            print(f'___Yade file: {path} is being loaded.')
            O.load(abspath+'/'+path)
            forceOnPipeList=[]
            for i in O.interactions:
                if i.id1 in boundaryDict['shearbox_members']:
                    forceOnPipeList.append([list(i.geom.contactPoint),list(-i.phys.normalForce-i.phys.shearForce)])
                elif i.id2 in boundaryDict['shearbox_members']:
                    forceOnPipeList.append([list(i.geom.contactPoint),list(i.phys.normalForce+i.phys.shearForce)])
            saver(forceOnPipeList,savePath)
            print(f'______forceOnPipe is saved to {savePath}.')
    n+=1


