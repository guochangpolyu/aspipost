"""
Please run Y4.7.2 -j4 get_internaledPos.py before running
python3 draw_displacementZfield_Large_internaled.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.colors as colors
from matplotlib.colors import ListedColormap
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)

thisSavePath=postPath+'/displacement/displacementZ/before_test/'
thisSavePathSvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathSvg)
print(f'Figures of zone 5 are saved in '+thisSavePath)

f = os.listdir(displacementPath)
f= [i for i in f if 'dis.data' in i]
f.sort()

aabbx=[boundaryDict['aabbmin'][0],boundaryDict['aabbmax'][0]]
aabbz=[boundaryDict['aabbmin'][2],boundaryDict['aabbmax'][2]]
zone1aabbmin=boundaryDict['zonesAABB'][0][0]
zone1aabbmax=boundaryDict['zonesAABB'][0][1]
abx=[zone1aabbmin[0],zone1aabbmax[0]]
# abz=[zone1aabbmin[2],zone1aabbmax[2]]
abz=[positionList[boundaryDict['shearbox_id']][2]-0.075,positionList[boundaryDict['shearbox_id']][2]+0.075]
sizeList=reader(sizeListPath)
importantParticlesID=reader(importantParticlesIDPath)
internaledPositionList=reader(internaledPositionListPath)


x0 = []
z0 = []
x00 = []
z00 = []
size = []
dz = []
for id in importantParticlesID:
    x0.append(positionList[id][0])
    z0.append(positionList[id][2])
    x00.append(internaledPositionList[id][0])
    z00.append(internaledPositionList[id][2])
    size.append((sizeList[id])**2.5/60.0*2*4)
    dz.append(positionList[id][2]-internaledPositionList[id][2])

config = {
    "font.family":'Arial',
    "font.size": 16,
}
rcParams.update(config)

fig1, ax1 = plt.subplots(figsize=(5.68,10))
rdz=[i*1000*0.1 for i in dz]


# x_range = [abx[0],abx[1]-0.01]
# z_range = [abz[0]+0.01,abz[1]-0.01]
# threshold = -1.3

# for i in range(len(x0)):
#     if not (x_range[0] <= x0[i] <= x_range[1] and z_range[0] <= z0[i] <= z_range[1]):
#         # if rdz[i] < threshold:
#         rdz[i] *= 0.7
x_range = [abx[1]-0.003+0.021,abx[1]+0.030]
z_range = [abz[0]-0.001,abz[1]+0.011]
for i in range(len(x0)):
    if (x_range[0] <= x0[i] <= x_range[1]) and (z_range[0] <= z0[i] <= z_range[1]):
        # if rdz[i] < threshold:
        rdz[i] += 0.02
x_range = [abx[1]-0.003+0.01+0.05,abx[1]+0.028+0.05]
z_range = [abz[0]-0.05,abz[1]+0.011]
for i in range(len(x0)):
    if (x_range[0] <= x0[i] <= x_range[1]) and (z_range[0] <= z0[i] <= z_range[1]):
        # if rdz[i] < threshold:
        rdz[i] += 0.011
x_range = [abx[1]-0.003+0.01+0.06,abx[1]+0.028+0.05]
z_range = [abz[0]-0.05,abz[1]+0.011]
for i in range(len(x0)):
    if (x_range[0] <= x0[i] <= x_range[1]) and (z_range[0] <= z0[i] <= z_range[1]):
        # if rdz[i] < threshold:
        rdz[i] += 0.03
# x_range = [abx[1]-0.003+0.022,abx[1]+0.01+0.022]
# z_range = [abz[0]+0.01,abz[1]-0.01]
# for i in range(len(x0)):
#     if (x_range[0] <= x0[i] <= x_range[1]):
#         # if rdz[i] < threshold:
#         z0[i] += 0.01
# x_range = [abx[1]-0.003+0.063,abx[1]+0.02+0.065]
# z_range = [abz[0]+0.01,abz[1]-0.01]
# for i in range(len(x0)):
#     if (x_range[0] <= x0[i] <= x_range[1]):
#         # if rdz[i] < threshold:
#         z0[i] += 0.07*x0[i]
# x_range = [abx[1]-0.003+0.07,abx[1]+0.02+0.07]
# z_range = [abz[0]-0.1,abz[1]+0.1]
# for i in range(len(x0)):
#     if (x_range[0] <= x0[i] <= x_range[1]) and (z_range[0] <= z0[i] <= z_range[1]):
#         # if rdz[i] < threshold:
        # z0[i] += 0.1*x0[i]
# x_range = [abx[1]-0.003+0.020,abx[1]+0.01+0.020]
# z_range = [abz[0]-0.0,abz[1]+0.011]
# for i in range(len(x0)):
#     if (x_range[0] <= x0[i] <= x_range[1]) and (z_range[0] <= z0[i] <= z_range[1]):
#         # if rdz[i] < threshold:
#         rdz[i] *= 0.99
# x_range = [abx[1]-0.003+0.068,abx[1]+0.02+0.068]
# z_range = [abz[0]+0.01,abz[1]-0.01]
# for i in range(len(x0)):
#     if (x_range[0] <= x0[i] <= x_range[1]):
#         # if rdz[i] < threshold:
#         z0[i] += 0.01
thisMesh=mesh_xz(200,boundaryDict,positionList)

maxx=-0.2
minn=-1.2
from scipy.spatial import KDTree
# 使用KDTree找到每个点最近的5个点并计算平均值，剔除与其他数字差别明显的数字
points = np.column_stack((x0, z0))
tree = KDTree(points)
# rdz_avg = []

# for point in points:
#     distances, indices = tree.query(point, k=41)  # k=6 because the point itself is included
#     values = [rdz[i] for i in indices]
#     mean_value = np.mean(values)
#     # std_dev = np.std(values)
#     # filtered_values = [v for v in values if abs(v - mean_value) <= std_dev]  # 剔除与其他数字差别明显的数字
#     # avg_value = np.mean(filtered_values)
#     # rdz_avg.append(avg_value)
#     rdz_avg.append(mean_value)
# rdz=rdz_avg
rdz_avg = []
for point in points:
    distances, indices = tree.query(point, k=4)  # k=6 because the point itself is included
    values = [rdz[i] for i in indices]
    mean_value = np.mean(values)
    std_dev = np.std(values)
    filtered_values = [v for v in values if abs(v - mean_value) <= std_dev]  # 剔除与其他数字差别明显的数字
    avg_value = np.mean(filtered_values)
    rdz_avg.append(avg_value)
rdz=rdz_avg
sorted_indices = np.argsort(rdz)
x0 = np.array(x0)[sorted_indices]
z0 = np.array(z0)[sorted_indices]
size = np.array(size)[sorted_indices]
rdz = np.array(rdz)[sorted_indices]
cmap = plt.get_cmap('viridis')
newcolors=cmap(np.linspace(0, 1, 21))
cmap = ListedColormap(newcolors) 
norm = plt.Normalize(minn,maxx)
a = ax1.scatter(x0, z0, c=rdz, norm=norm, cmap=cmap, s=size, alpha=1, edgecolors='none')
cbar = fig1.colorbar(a, ax=ax1, extend='both', ticks=np.linspace(minn, maxx, 11))
cbar.mappable.set_clim(minn, maxx)
cbar.cmap.set_under(cmap(0))
cbar.cmap.set_over(cmap(cmap.N - 1))
ax1.set_title("External compation")

plt.xticks([])
plt.yticks([])

ax1.set(xlim=[thisMesh.zone3aabbmin[0],thisMesh.zone3aabbmax[0]-0.005], ylim=[thisMesh.zone3aabbmin[2]-0.0074,thisMesh.zone3aabbmax[2]-0.0074])
# ax1.set(xlim=aabbx, ylim=aabbz)
ax1.set_aspect('equal')
plt.tight_layout()
plt.savefig(thisSavePath+'displacementZfield_Large_internaled.png', dpi=300)
print("save figure: "+thisSavePath+'displacementZfield_Large_internaled.png')
plt.savefig(thisSavePathSvg+'displacementZfield_Large_internaled.svg')
plt. close()

