"""
Please run Y4.7.2 -j4 get_pressureMatrixZone5.py before running
python3 draw_pressureMatrixPrincipal.py
"""
##########################################  
import sys
sys.path.append('')
import numpy as np
from myCommonSavers import *
from myCommonFunction import *
from setting import *
import matplotlib.pyplot as plt
from matplotlib import rcParams

### Read necessary base data
boundaryDict=reader(boundaryDictPath)
positionList=reader(positionListPath)
ppPosDict=reader(ppPosDictPath)

### define and create path for saving
thisSavePath=pressureMatrix_zone3_principal_path
thisSavePathsvg=thisSavePath+'svg/'
checkPath(thisSavePath)
checkPath(thisSavePathsvg)

### define and get path for reading analysed data
thisReadName=pressureMatrixZone5Path
dataName='pressureMatrixZone5'
f = os.listdir(thisReadName)
f= [i for i in f if dataName+'.data' in i]
f.sort()

### initialize a mesh
thisMesh=mesh_xz(30,boundaryDict,positionList,smallorlarge=0)

### common plt setting
config = {
    "font.family":'Arial',
    "font.size": 16,
    }


gap=10
n=0
for p in f:
    if ((n%gap==0 and n<=200)or n==1):
        pathHead=p.replace(dataName+'.data','')
        ppPos=ppPosDict[pathHead]
        displacement=int(pathHead)
        ppDisplacement=displacement/10000.0
        print(f'Now analyse the condition of axial displacement is {displacement}')

        thisMesh.updateSpace([ppPos[0],ppPos[2]],ppR,calTouchedMesh=1)
        pressures=thisMesh.meshList(0.0)
        pressures2=thisMesh.meshList(0.0)
        shearStress=thisMesh.meshList(0.0)

        sigma_1=thisMesh.meshList(0.0)
        sigma_2=thisMesh.meshList(0.0)
        theta_p_deg=thisMesh.meshList(0.0)

        pressureMatrixList=reader(thisReadName+p)
        for contactPoint,pressureMatrix in pressureMatrixList:
            c=thisMesh.meshilize(periodicialize(contactPoint,boundaryDict_cell=boundaryDict['cell'],xyz='xyz'))
            if (0<=c[0]<thisMesh.xMeshNum) and (0<=c[2]<thisMesh.zMeshNum):
                i=int(c[2])
                j=int(c[0])
                pressures[i][j]+=pressureMatrix[2][2]
                pressures2[i][j]+=pressureMatrix[0][0]
                shearStress[i][j]+=pressureMatrix[1][2]
        pressures=[[np.nan if thisMesh.space[i][j]==np.nan or thisMesh.space[i][j]==0.0 else -pressures[i][j]/thisMesh.space[i][j]/1000 for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]
        window_size=4
        pressures=moving_average_2d_with_weight(np.array(pressures),np.array(thisMesh.space), window_size=window_size)

        pressures2=[[np.nan if thisMesh.space[i][j]==np.nan or thisMesh.space[i][j]==0.0 else -pressures2[i][j]/thisMesh.space[i][j]/1000 for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]
        pressures2=moving_average_2d_with_weight(np.array(pressures2),np.array(thisMesh.space), window_size=window_size)

        shearStress=[[np.nan if thisMesh.space[i][j]==np.nan or thisMesh.space[i][j]==0.0 else -shearStress[i][j]/thisMesh.space[i][j]/1000 for j in range(thisMesh.xMeshNum)]for i in range(thisMesh.zMeshNum)]
        shearStress=moving_average_2d_with_weight(np.array(shearStress),np.array(thisMesh.space), window_size=window_size)

        rcParams.update(config)
        fig1, ax1 = plt.subplots(figsize=(7,10))

        for i in range(thisMesh.zMeshNum):
            for j in range(thisMesh.xMeshNum):
                if pressures[i][j] == np.nan: continue
                sigma_1[i][j], sigma_2[i][j], theta_p_deg[i][j] = principal_stress_and_direction(pressures[i][j], pressures2[i][j], shearStress[i][j])
                # 主应力方向线的终点坐标
                length=0.008
                dx1 = math.sin(math.radians(theta_p_deg[i][j]))*length*sigma_1[i][j]
                dz1 = math.cos(math.radians(theta_p_deg[i][j]))*length*sigma_1[i][j]
                
                # 偏应力方向线的终点坐标（与主应力方向垂直）
                dx2 = math.sin(math.radians(theta_p_deg[i][j] + 90))*length*sigma_2[i][j]
                dz2 = math.cos(math.radians(theta_p_deg[i][j] + 90))*length*sigma_2[i][j]
                x=j+0.5
                z=i+0.5
                # 绘制偏应力方向线
                a=ax1.plot([x - dx2, x + dx2 ], [z - dz2, z + dz2], color=plt.cm.viridis(sigma_2[i][j] / (math.ceil((simulationPressure*2)/10.0)*10)),linewidth=3)

                # 绘制主应力方向线
                a=ax1.plot([x - dx1, x + dx1], [z - dz1, z + dz1], color=plt.cm.viridis(sigma_1[i][j] / (math.ceil((simulationPressure*2)/10.0)*10)),linewidth=3)
                


        # a=ax1.imshow(sigma_1,  interpolation='bilinear')
        # a=ax1.imshow(pressures)
        ppR_meshed=thisMesh.meshilize_length(ppR)
        ppC_meshed=thisMesh.meshilize(ppPos)
        draw_half_circle_outline(ax1, [ppC_meshed[0],ppC_meshed[2]], ppR_meshed, 8,edgecolor='black',fill=1)

        norm = plt.Normalize(0,math.ceil((simulationPressure*2)/10.0)*10)
        sm = plt.cm.ScalarMappable(cmap='viridis', norm=norm)
        sm.set_array([])
        fig1.colorbar(sm, ax=ax1,extend='max')
        # cbar=fig1.colorbar(a, ax=ax1,extend='max')
        
        # cbar.mappable.set_clim(0,math.ceil((simulationPressure*2)/10.0)*10)

        ax1.set_title('${u}$ = '+ '%.1f'%(float(ppDisplacement)*1000)+" mm")

        plt.xticks([])
        plt.yticks([])

        ax1.set(xlim=[thisMesh.zone3aabbmin_meshed[0],thisMesh.zone3aabbmax_meshed[0]], ylim=[thisMesh.zone3aabbmin_meshed[2],thisMesh.zone3aabbmax_meshed[2]])
        for spine in ax1.spines.values():
            spine.set_zorder(15)
        ax1.set_aspect('equal')
        plt.tight_layout()

        figName=thisSavePath+pathHead
        fig1.savefig(figName+'.png', dpi=300)
        print("save figure: "+figName+'.png')
        fig1.savefig(thisSavePathsvg+pathHead+'.svg')
        plt.close()

    n+=1
